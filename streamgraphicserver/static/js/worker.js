importScripts('https://cdn.jsdelivr.net/npm/socket.io@4.1.2/client-dist/socket.io.min.js')

var version = '12.0.1-rc1';

var socket = io("/websocket", {
    jsonp: false
});
var channel = new BroadcastChannel('sw-socketio-msgs');

channel.addEventListener('message', event => {});

socket.on('connect', () => {
    console.log(socket.id);
});

socket.on('connect', function () {
    console.info('connect');
});
socket.on('connect_error', function (error) {
    console.info('connect_error');
});
socket.on('disconnect', function (reason) {
    console.info('disconnect');
});

socket.io.on('reconnect', function (attemptNumber) {
    console.info('reconnect');
});
socket.io.on('reconnect_error', function (error) {
    console.info('reconnect_error');
});
socket.io.on('reconnect_failed', function () {
    console.info('reconnect_failed');
});
socket.io.on('error', function (error) {
    console.info('error');
});

socket.on('update', function (msg) {
    channel.postMessage({type: 'update', msg: msg});
});
socket.on('update_multiple', function (msg) {
    channel.postMessage({type: 'update_multiple', msg: msg});
});
socket.on('update_compressed', function (msg) {
    channel.postMessage({type: 'update_compressed', msg: msg});
});
socket.on('update_multiple_compressed', function (msg) {
    channel.postMessage({type: 'update_multiple_compressed', msg: msg});
});
socket.on('alert_overlay', function (msg) {
    channel.postMessage({type: 'alert_overlay', msg: msg});
});
