channel.addEventListener('message', event => {
    var msg_type = event.data.type;
    var msg = event.data.msg;

    if (msg_type === 'alert_overlay') {

        console.info('alert_overlay ' + msg['alert_channel'], msg);

        if (msg['alert_channel'] === alert_channel
        ) {
            var event_text = "";

            event_text += msg['text_left'];

            if (msg['image'] !== "" || msg['fa_icon'] !== "") {
                if (msg['image'] !== "") {
                    if (msg['image'].endsWith(".svg")) {
                        event_text += ' <img src="' + msg['image'] + '" onload="SVGInject(this)"/> ';
                    } else {
                        event_text += ' <img src="' + msg['image'] + '"/> ';
                    }
                }
                if (msg['fa_icon'] !== "") {
                    event_text += ' <i class="' + msg['fa_icon'] + '"></i> ';
                }

            } else {
                if ((msg['text_left'] !== "" && msg['text_right'] === "") || (msg['text_left'] === "" && msg['text_right'] !== "")) {
                    //pass - Do not need a default separator.
                } else {
                    event_text += msg['separator'];
                }
            }

            event_text += msg['text_right'];


            add_alert_overlay(event_text, msg);
        }
    }
});

var added_logs = 0;

function add_alert_overlay(text, msg) {
    added_logs += 1;

    var new_event_div = document.createElement('div');
    new_event_div.classList.add('event');
    new_event_div.innerHTML = text;
    new_event_div.id = "event_" + added_logs;
    new_event_div.style.backgroundColor = msg['bg_color'];
    new_event_div.style.color = msg['text_color'];
    new_event_div.style.fill = msg['text_color'];

    document.getElementById("alert_overlay").appendChild(new_event_div);

    setTimeout(remove_alert_overlay, msg['time'], added_logs);
}

function remove_alert_overlay(num) {
    var remove_div = document.getElementById("event_" + num);

    remove_div.style.opacity = 0;
    remove_div.style.scale = 0;
    remove_div.style.marginBottom = "-2em";
    remove_div.style.marginRight = "-2em";

    setTimeout(function () {
        document.getElementById("alert_overlay").removeChild(document.getElementById("event_" + num));
    }, 500);
}