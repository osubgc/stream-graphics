const SVG_NS = "http://www.w3.org/2000/svg";

function overlay_init(overlay_name) {
    if (window.console) console.log('Loading overlay for:', overlay_name);

    // Should not be needed as it should already exist, but just in case.
    if (!document.getElementById('overlay/' + overlay_name)) {
        var new_elem = document.createElement('div');

        new_elem.id = 'overlay/' + overlay_name;
        new_elem.classList.add('overlay');

        document.body.appendChild(new_elem);
    }

    if (data.has('overlay/' + overlay_name + '/layers')) {
        data.get('overlay/' + overlay_name + '/layers').split(',').forEach(function (layer_name) {
            layer_init(overlay_name, layer_name);
        });
    }

    if (data.has('overlay/' + overlay_name + '/display')) {
        var overlay_div = document.getElementById('overlay/' + overlay_name);

        overlay_div.setAttribute('data-sg-display', data.get('overlay/' + overlay_name + '/display'));
    }
}

function layer_init(overlay_name, layer_name) {
    if (window.console) console.log('Loading layer for:', layer_name);

    if (!document.getElementById('layer/' + layer_name)) {
        var new_elem = document.createElement('div');

        new_elem.id = 'layer/' + layer_name;
        new_elem.classList.add('layer');

        document.getElementById('overlay/' + overlay_name).appendChild(new_elem);

    }

    if (data.has('layer/' + layer_name + '/aspect')) {
        let layer_div = document.getElementById('layer/' + layer_name);

        layer_div.setAttribute('data-sg-aspect', data.get('layer/' + layer_name + '/aspect'));
    }

    if (data.has('layer/' + layer_name + '/aspect-pos')) {
        let layer_div = document.getElementById('layer/' + layer_name);

        layer_div.setAttribute('data-sg-aspect-pos', data.get('layer/' + layer_name + '/aspect-pos'));
    }

    if (data.has('layer/' + layer_name + '/display')) {
        let layer_div = document.getElementById('layer/' + layer_name);

        layer_div.setAttribute('data-sg-display', data.get('layer/' + layer_name + '/display'));
    }

    if (data.has('layer/' + layer_name + '/components')) {
        data.get('layer/' + layer_name + '/components').split(',').forEach(function (component_name) {
            component_init(layer_name, component_name);
        });
    }


}

function component_init(layer_name, component_name) {
    if (window.console) console.log('Loading component for:', component_name);

    if (!document.getElementById('component/' + component_name)) {
        var new_elem = document.createElement('div');

        new_elem.id = 'component/' + component_name;
        new_elem.classList.add('sg');
        new_elem.setAttribute('data-sg-display', 'hide');
        get_all_component_styles(component_name).forEach(function (style) {
            new_elem.setAttribute('data-sg-' + style.split('/')[3], data.get(style));
        });

        document.getElementById('layer/' + layer_name).appendChild(new_elem);

        if (data.has('component/' + component_name + '/lang-override')) {
            new_elem.setAttribute('lang-override', data.get('component/' + component_name + '/lang-override'));
        } else {
            new_elem.setAttribute('lang-override', '');
        }

        if (data.has('component/' + component_name + '/mode')) {
            new_elem.setAttribute('data-mode', data.get('component/' + component_name + '/mode'));
        } else {
            new_elem.setAttribute('data-mode', 'raw');
        }

        if (data.has('component/' + component_name + '/custom-style')) {
            custom_style_path = path_for_lang('data/' + data.get('component/' + component_name + '/custom-style'), new_elem);
            new_elem.setAttribute('style', data.get(custom_style_path));
            new_elem.setAttribute('sg-custom-style', 'data/' + data.get('component/' + component_name + '/custom-style'));
        }

        if (data.has('component/' + component_name + '/custom-css')) {

            custom_style_path = path_for_lang('data/' + data.get('component/' + component_name + '/custom-css'), new_elem);

            Object.assign(new_elem.style, filtered_data(custom_style_path));
            new_elem.setAttribute('sg-custom-css', 'data/' + data.get('component/' + component_name + '/custom-css'));
        }

        if (data.has('component/' + component_name + '/custom-options')) {
            new_elem.setAttribute('sg-custom-options', 'data/' + data.get('component/' + component_name + '/custom-options'));
        }

        var inner_div = document.createElement('div');

        inner_div.classList.add('inner-component');
        new_elem.appendChild(inner_div);

        if (data.has('component/' + component_name + '/data')) {
            new_elem.setAttribute('sg-data-name', 'data/' + data.get('component/' + component_name + '/data'));
            data_init(data.get('component/' + component_name + '/data'));
        }
    }
}

function data_init(data_name) {
    display_data_update('data/' + data_name);
}

function display_overlay_update(path) {
    var split_path = path.split('/');

    if (split_path[2] === 'layers') {
        current_layers = Array.from(document.getElementById('overlay/' + split_path[1]).children).map(element => { return element.id.replace('layer/', ''); });

        console.log('cur lay', current_layers);
        console.log('new lay', data.get(path).split(','));

        current_layers.forEach(function (value) {
            if (!data.get(path).split(',').includes(value)) {
                console.log('removing', value);
                document.querySelectorAll('#layer\\/' + value + ' .video-js').forEach(function (vid_elem) {
                    vid_elem_id = vid_elem.id;
                    console.log('remove', value);
                    console.log('remove vid', videojs(vid_elem));
                    videojs(vid_elem).dispose();
                    delete videojs.players[vid_elem_id];
                });
                document.getElementById('layer/' + value).remove();
            }
        });
        data.get(path).split(',').forEach(function (value) {
            layer_init(split_path[1], value);
        });
    } else if (split_path[2] === 'display') {
        var overlay_div = document.getElementById('overlay/' + split_path[1]);

        overlay_div.setAttribute('data-sg-display', data.get(path));
    }
}

function display_layer_update(path) {
    var split_path = path.split('/');
    if (split_path[2] === 'components') {
        current_components = Array.from(document.getElementById('layers/' + split_path[1]).children).map(element => { return element.id.replace('component/', ''); });

        console.log('cur comp', current_components);
        console.log('new comp', data.get(path).split(','));

        current_components.forEach(function (value) {
            if (!data.get(path).split(',').includes(value)) {
                console.log('removing', value);
                document.querySelectorAll('#component\\/' + value + ' .video-js').forEach(function (vid_elem) {
                    vid_elem_id = vid_elem.id;
                    console.log('remove', value);
                    console.log('remove vid', videojs(vid_elem));
                    videojs(vid_elem).dispose();
                    delete videojs.players[vid_elem_id];
                });
                document.getElementById('component/' + value).remove();
            }
        });
        data.get(path).split(',').forEach(function (value) {
            component_init(split_path[1], value);
        });
    } else if (split_path[2] === 'aspect') {
        let layer_div = document.getElementById('layer/' + split_path[1]);

        layer_div.setAttribute('data-sg-aspect', data.get(path));
    } else if (split_path[2] === 'aspect-pos') {
        let layer_div = document.getElementById('layer/' + split_path[1]);

        layer_div.setAttribute('data-sg-aspect-pos', data.get(path));
    } else if (split_path[2] === 'display') {
        let layer_div = document.getElementById('layer/' + split_path[1]);

        layer_div.setAttribute('data-sg-display', data.get(path));
    }
}

function display_component_update(path) {
    var split_path = path.split('/');
    var elem = document.getElementById('component/' + split_path[1]);

    if (split_path[2] === 'style') {
        if (split_path.length === 4) {
            elem.setAttribute('data-sg-' + split_path[3], data.get(path));
        }
    } else if (split_path[2] === 'data') {
        elem.getElementsByClassName('inner-component')[0].innerHTML = '';
        elem.setAttribute('sg-data-name', 'data/' + data.get(path));
        console.log('data-reinit', split_path[1], path);
        data_init(data.get(path));
    } else if (split_path[2] === 'lang-override') {
        elem.setAttribute('lang-override', data.get(path));
        if (elem.hasAttribute('sg-data-name')) {
            let data_path = elem.getAttribute('sg-data-name');
            elem.getElementsByClassName('inner-component')[0].innerHTML = '';
            console.log('data-reinit', split_path[1], data_path);

            if (elem.hasAttribute('sg-custom-style')) {
                let custom_style_path = elem.getAttribute('sg-custom-style');
                custom_style_path = path_for_lang(custom_style_path, elem);
                console.log('custom-style-reinit', split_path[1], custom_style_path);
                elem.setAttribute('style', data.get(custom_style_path));
            }

            if (elem.hasAttribute('sg-custom-css')) {
                let custom_style_path = elem.getAttribute('sg-custom-css');
                custom_style_path = path_for_lang(custom_style_path, elem);
                console.log('custom-css-reinit', split_path[1], custom_style_path);
                Object.assign(elem.style, filtered_data(custom_style_path));
            }

            render_data_display(elem, data_path);
        }
    } else if (split_path[2] === 'mode') {
        if (elem) {
            elem.setAttribute('data-mode', data.get(path));
        }
        if (elem.hasAttribute('sg-data-name')) {
            let data_path = elem.getAttribute('sg-data-name');
            if (data_path in janusObjects) {
                endJanusStream(data_path);
            }
            //elem.getElementsByClassName('inner-component')[0].innerHTML = '';
            display_data_update(data_path);
        }
    } else if (split_path[2] === 'custom-style') {
        if (elem) {
            custom_style_path = path_for_lang('data/' + data.get(path), elem);
            elem.setAttribute('style', data.get(custom_style_path));
            elem.setAttribute('sg-custom-style', path);
        }
    } else if (split_path[2] === 'custom-css') {
        if (elem) {
            custom_style_path = path_for_lang('data/' + data.get(path), elem);
            Object.assign(elem.style, filtered_data(custom_style_path));
            elem.setAttribute('sg-custom-css', path);
        }
    } else if (split_path[2] === 'custom-options') {
        if (elem) {
            elem.setAttribute('sg-custom-options', 'data/' + data.get(path));
        }
    }
}

function getWebcams() {
    return navigator.mediaDevices.enumerateDevices()
        .then((mediaDevices) => {
            return mediaDevices.filter((device) => {
                return device.kind === 'videoinput';
            });
        });
}

function getWebcamMatch(deviceLabel) {
    return navigator.mediaDevices.enumerateDevices()
        .then((mediaDevices) => {
            return mediaDevices.filter((device) => {
                return device.label === deviceLabel;
            });
        });
}

function startWebcamStream(deviceId, outputElem, path) {
    var constraints = {
        audio: false,
        video: {
            frameRate: {
                ideal: 60
            },
            width: {
                ideal: 1920
            },
            height: {
                ideal: 1080
            },
            deviceId: {
                exact: deviceId
            }
        }
    };

    navigator.mediaDevices.getUserMedia(constraints)
        .then((mediaStream) => {
            var videoElem = document.createElement('video');
            videoElem.id = path;
            videoElem.style.height = '100% !important';
            videoElem.style.width = 'auto !important';
            videoElem.muted = true;
            videoElem.autoplay = true;
            setVideoStream(videoElem, mediaStream);
            outputElem.appendChild(videoElem);
        });
}

function startWebcamViaName(deviceName, outputElem, path) {
    getWebcamMatch(deviceName)
        .then((webcamDevice) => {
            startWebcamStream(webcamDevice[0].deviceId, outputElem, path);
        });
}

function setVideoStream(videoElem, mediaStream) {
    try {
        videoElem.srcObject = mediaStream;
    } catch (error) {
        videoElem.src = window.URL.createObjectURL(mediaStream);
    }
}


function sanitize(string) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#x27;',
        "/": '&#x2F;',
    };
    var reg = /[&<>"'/]/ig;
    return string.replace(reg, (match) => (map[match]));
}

// From: https://github.com/leeoniya/uPlot/issues/642#issuecomment-1002719927
let can = document.createElement("canvas");
let ctx = can.getContext("2d");

function scaleGradient(u, scaleKey, ori, scaleStops, discrete = false) {
    let scale = u.scales[scaleKey];

    // we want the stop below or at the scaleMax
    // and the stop below or at the scaleMin, else the stop above scaleMin
    let minStopIdx;
    let maxStopIdx;

    for (let i = 0; i < scaleStops.length; i++) {
        let stopVal = scaleStops[i][0];

        if (stopVal <= scale.min || minStopIdx == null)
            minStopIdx = i;

        maxStopIdx = i;

        if (stopVal >= scale.max)
            break;
    }

    if (minStopIdx == maxStopIdx)
        return scaleStops[minStopIdx][1];

    let minStopVal = scaleStops[minStopIdx][0];
    let maxStopVal = scaleStops[maxStopIdx][0];

    if (minStopVal == -Infinity)
        minStopVal = scale.min;

    if (maxStopVal == Infinity)
        maxStopVal = scale.max;

    let minStopPos = u.valToPos(minStopVal, scaleKey, true);
    let maxStopPos = u.valToPos(maxStopVal, scaleKey, true);

    let range = minStopPos - maxStopPos;

    let x0, y0, x1, y1;

    if (ori == 1) {
        x0 = x1 = 0;
        y0 = minStopPos;
        y1 = maxStopPos;
    } else {
        y0 = y1 = 0;
        x0 = minStopPos;
        x1 = maxStopPos;
    }

    let grd = ctx.createLinearGradient(x0, y0, x1, y1);

    let prevColor;

    for (let i = minStopIdx; i <= maxStopIdx; i++) {
        let s = scaleStops[i];

        let stopPos = i == minStopIdx ? minStopPos : i == maxStopIdx ? maxStopPos : u.valToPos(s[0], scaleKey, true);
        let pct = (minStopPos - stopPos) / range;

        if (discrete && i > minStopIdx)
            grd.addColorStop(pct, prevColor);

        grd.addColorStop(pct, prevColor = s[1]);
    }

    return grd;
}
// End from.


var charts = {};


function display_data_update(path) {
    document.querySelectorAll("[sg-custom-style='" + path + "']").forEach(function (element) {
        element.style = data.get(path_for_lang(path, element));
    });
    document.querySelectorAll("[sg-custom-style='" + path + "/*']").forEach(function (element) {
        element.style = data.get(path_for_lang(path, element));
    });

    // Updated setting path.
    document.querySelectorAll("[sg-custom-options='" + path + "']").forEach(function (element) {
        render_data_display(element, element.getAttribute('sg-data-name'));
        parse_options(element.getAttribute('sg-custom-options'), true);
    });

    // New settings.
    document.querySelectorAll("[sg-custom-options]").forEach(function (element) {
        if (element.hasAttribute('sg-monitored-data') && !element.getAttribute('sg-monitored-data').includes(path)) {
            // New setting.
            if (path.startsWith(element.getAttribute('sg-custom-options'))) {
                // Data part of the sg-custom-options component.
                console.log('updated chart option', path);
                render_data_display(element, element.getAttribute('sg-data-name'));
                parse_options(element.getAttribute('sg-custom-options'), true);
            }
        }
    });

    // New settings.
    document.querySelectorAll("[sg-custom-css]").forEach(function (element) {
        if (path.includes(element.getAttribute('sg-custom-css'))) {
            Object.assign(element.style, filtered_data(element.getAttribute('sg-custom-css')));
        }
    });

    // Updated data for setting references.
    document.querySelectorAll("[sg-monitored-data*='" + path + "']").forEach(function (element) {
        //console.log('monitored-update', path);

        // Is this a chart?
        if (element.hasAttribute('data-mode') && element.getAttribute('data-mode').includes("chart")) {
            if (element.hasAttribute('sg-custom-options') && path.startsWith(element.getAttribute('sg-custom-options'))) {
                // Data part of the sg-custom-options component.
                //console.log('updated chart option', path);
                render_data_display(element, element.getAttribute('sg-data-name'));
            } else {
                // Data referenced in the options.
                //console.log('updated chart data', path);
                let chart_options = parse_options(element.getAttribute('sg-custom-options'));

                let chart_data = [];

                let x_data = data.get(chart_options['x-data']).split(',').map(Number);
                chart_data.push(x_data);

                chart_options['y-data'].split(',').forEach(function (value) {
                    let y_data = data.get(value).split(',').map(Number);
                    chart_data.push(y_data);
                });

                charts[element.getAttribute('sg-data-name')].setData(chart_data);

                if ("team-colors" in chart_options && (path === chart_options['team-colors'][0] || path === chart_options['team-colors'][1])) {
                    console.log('team-colors-update');
                    render_data_display(element, element.getAttribute('sg-data-name'));
                }
            }
        }
    });

    document.querySelectorAll("[sg-data-name='" + path + "']").forEach(function (element) {
        render_data_display(element, path);
    });
}


function options_to_monitor(key) {
    // let filtered_data = Object.entries(data).filter(([k, v]) => k.startsWith(key + '/'));
    // let filtered_data_values = filtered_data.filter(([k, v]) => v.startsWith('data/'));
    // let monitored_options_keys = filtered_data.map(([k, v]) => k);
    // let monitored_options_values = filtered_data_values.map(([k, v]) => v);
    // let monitored_option_list = monitored_options_keys.concat(monitored_options_values).join(',');

    let filtered_data = Array.from(data).filter(([k, v]) => k.startsWith(key + '/'));
    let filtered_data_values = filtered_data.filter(([k, v]) => v.startsWith('data/'));
    let monitored_options_keys = Object.keys(filtered_data);
    let monitored_options_values = Object.values(filtered_data_values);
    let monitored_option_list = monitored_options_keys.concat(monitored_options_values).join(',');

    return monitored_option_list;
}

var options_cache = {};

function parse_options(key, refresh_cache = false) {
    if (refresh_cache || !(key in options_cache)) {
        options_cache[key] = unflatten(filtered_data(key));
    }

    return options_cache[key];
}

function path_for_lang(path, elem) {
    let lang = default_lang;

    if (url_params.has('lang')) {
        lang = url_params.get('lang').toLowerCase();
    }

    if (elem && elem.hasAttribute('lang-override') && elem.getAttribute('lang-override') != '') {
        lang = elem.getAttribute('lang-override').toLowerCase();
        console.log('lang-override', path, lang);
    }

    if (path + '/' + lang in data) {
        path = path + '/' + lang;
    } else if (path + '/' + default_lang in data) {
        path = path + '/' + default_lang;
    }

    return path;
}



function render_data_display(element, path) {
    let inner_element = element.getElementsByClassName('inner-component')[0];
    let options = null;

    path = path_for_lang(path, element);

    if (!(data.has(path))) {
        console.log('no-data', path);
        return;
    }

    if (element.hasAttribute('sg-custom-options')) {
        options = parse_options(element.getAttribute('sg-custom-options'));
        let monitor_options = options_to_monitor(element.getAttribute('sg-custom-options'));

        element.setAttribute('sg-monitored-data', monitor_options);
    }

    if (element.hasAttribute('data-mode')) {
        let mode = element.getAttribute('data-mode');

        if (mode === 'raw') {
            inner_element.innerText = data.get(path);
        } else if (mode === 'text') {
            inner_element.innerHTML = sanitize(data.get(path)).replace(/\\n/g, '<br/>');
        } else if (mode === 'text-newline') {
            inner_element.innerHTML = sanitize(data.get(path)).replace(/\\n/g, '<br/>');
        } else if (mode === 'html') {
            inner_element.innerHTML = data.get(path);
        } else if (mode === 'markdown') {
            inner_element.innerHTML = marked.parseInline(data.get(path).replace(/\\n/g, '<br/>'));
        } else if (mode === 'chart-line') {
            console.log('uplot', options);

            inner_element.innerText = '';
            let new_elem = document.createElement('div');
            new_elem.style.width = '100%';
            new_elem.style.height = '100%';
            new_elem.id = path.replace(/\//gi, '');
            inner_element.appendChild(new_elem);

            requestAnimationFrame(() => {
                console.log('RENDERED');
                console.log('chart-size-rendered?', new_elem.offsetWidth, new_elem.offsetHeight);
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            });

            console.log('chart-size', new_elem.offsetWidth, new_elem.offsetHeight);
            computed_styles = getComputedStyle(new_elem);
            console.log('chart-size-computed', computed_styles.width, computed_styles.height);

            let chart_data = [];
            let chart_series_opts = [];
            let chart_axis_opts = [];

            console.log(options.axis);

            let x_data = data.get(options['x-data']).split(',').map(Number);
            chart_data.push(x_data);
            chart_series_opts.push({});

            for (let axis in ['x', 'y']) {
                chart_axis_opts.push({
                    stroke: flatten(options)['axis/' + axis + '/color'] || "#cccccc",
                    grid: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    ticks: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    show: varToBoolean(flatten(options)['axis/' + axis + '/show'], true),
                });
            }

            options['y-data'].split(',').forEach(function (value, index) {
                index = index + 1;
                let y_data = data.get(value).split(',').map(Number);
                chart_data.push(y_data);

                console.log('y index', index);
                chart_series_opts.push({
                    label: flatten(options)['name/y' + index] || "",
                    width: 1.5,
                    stroke: flatten(options)['color/y' + index] || ""
                });
            });

            console.log(chart_data);

            chart_config = {
                width: new_elem.offsetWidth,
                height: new_elem.offsetHeight,
                series: chart_series_opts,
                scales: {
                    "x": {
                        time: false,
                    }
                },
                axes: chart_axis_opts,
            };

            console.log('uplot', chart_config);

            charts[path] = uPlot(chart_config, chart_data, new_elem);

            new ResizeObserver(() => {
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            }).observe(new_elem);
        } else if (mode === 'chart-line-color-2') {
            console.log('uplot', options);

            inner_element.innerText = '';
            let new_elem = document.createElement('div');
            new_elem.style.width = '100%';
            new_elem.style.height = '100%';
            new_elem.id = path.replace(/\//gi, '');
            inner_element.appendChild(new_elem);

            requestAnimationFrame(() => {
                console.log('RENDERED');
                console.log('chart-size-rendered?', new_elem.offsetWidth, new_elem.offsetHeight);
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            });

            console.log('chart-size', new_elem.offsetWidth, new_elem.offsetHeight);
            computed_styles = getComputedStyle(new_elem);
            console.log('chart-size-computed', computed_styles.width, computed_styles.height);

            let chart_data = [];
            let chart_series_opts = [];
            let chart_axis_opts = [];

            console.log(options.axis);

            let x_data = data.get(options['x-data']).split(',').map(Number);
            chart_data.push(x_data);
            chart_series_opts.push({});

            ['x', 'y'].forEach((axis) => {
                chart_axis_opts.push({
                    stroke: flatten(options)['axis/' + axis + '/color'] || "#cccccc",
                    grid: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    ticks: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    points: {
                        show: false,
                    },
                    show: varToBoolean(flatten(options)['axis/' + axis + '/show'], true),
                });

                if ((flatten(options)['axis/' + axis + '/type'] || "") === "time") {
                    console.log('axis-type', axis, 'time');
                    chart_axis_opts[chart_axis_opts.length - 1].values = (self, ticks) => ticks.map(rawValue => {
                        minutes = Math.floor(rawValue / 60);
                        seconds = rawValue % 60;

                        return minutes + ':' + String(seconds).padStart(2, "0");
                    });
                }
            });

            options['y-data'].split(',').forEach(function (value, index) {
                index = index + 1;
                let y_data = data.get(value).split(',').map(Number);
                chart_data.push(y_data);

                console.log('y index', index);
                chart_series_opts.push({
                    label: flatten(options)['name/y' + index] || "",
                    width: 1.5,
                    fill: (u, seriesIdx) => {
                        let s = u.series[seriesIdx];
                        let sc = u.scales[s.scale];

                        return scaleGradient(u, s.scale, 1, [
                            [sc.min, "#DD6666BB"],
                            [0, "#DD6666BB"],
                            [0, "white"],
                            [0, "#6666DDDD"],
                            [sc.max, "#6666DDDD"],
                        ]);
                    },
                    stroke: '#DDDDDD'
                });
            });

            console.log(chart_data);

            chart_config = {
                width: new_elem.offsetWidth,
                height: new_elem.offsetHeight,
                series: chart_series_opts,
                scales: {
                    "x": {
                        time: false
                    }
                },
                axes: chart_axis_opts,
            };

            console.log('uplot', chart_config);

            charts[path] = uPlot(chart_config, chart_data, new_elem);

            new ResizeObserver(() => {
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            }).observe(new_elem);
        } else if (mode === 'chart-line-color-2-date') {
            console.log('uplot', options);

            inner_element.innerText = '';
            let new_elem = document.createElement('div');
            new_elem.style.width = '100%';
            new_elem.style.height = '100%';
            new_elem.id = path.replace(/\//gi, '');
            inner_element.appendChild(new_elem);

            requestAnimationFrame(() => {
                console.log('RENDERED');
                console.log('chart-size-rendered?', new_elem.offsetWidth, new_elem.offsetHeight);
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            });

            console.log('chart-size', new_elem.offsetWidth, new_elem.offsetHeight);
            computed_styles = getComputedStyle(new_elem);
            console.log('chart-size-computed', computed_styles.width, computed_styles.height);

            let chart_data = [];
            let chart_series_opts = [];
            let chart_axis_opts = [];

            console.log(options.axis);

            let x_data = data.get(options['x-data']).split(',').map(Number);
            chart_data.push(x_data);
            chart_series_opts.push({});

            ['x', 'y'].forEach((axis) => {
                chart_axis_opts.push({
                    stroke: flatten(options)['axis/' + axis + '/color'] || "#cccccc",
                    grid: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    ticks: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    points: {
                        show: false,
                    },
                    show: varToBoolean(flatten(options)['axis/' + axis + '/show'], true),
                });
            });

            options['y-data'].split(',').forEach(function (value, index) {
                index = index + 1;
                let y_data = data.get(value).split(',').map(Number);
                chart_data.push(y_data);

                console.log('y index', index);
                chart_series_opts.push({
                    label: flatten(options)['name/y' + index] || "",
                    width: 1.5,
                    fill: (u, seriesIdx) => {
                        let s = u.series[seriesIdx];
                        let sc = u.scales[s.scale];

                        return scaleGradient(u, s.scale, 1, [
                            [sc.min, "#DD6666BB"],
                            [0, "#DD6666BB"],
                            [0, "white"],
                            [0, "#6666DDDD"],
                            [sc.max, "#6666DDDD"],
                        ]);
                    },
                    stroke: '#DDDDDD'
                });
            });

            console.log(chart_data);

            chart_config = {
                width: new_elem.offsetWidth,
                height: new_elem.offsetHeight,
                series: chart_series_opts,
                axes: chart_axis_opts,
            };

            console.log('uplot', chart_config);

            charts[path] = uPlot(chart_config, chart_data, new_elem);

            new ResizeObserver(() => {
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            }).observe(new_elem);
        } else if (mode === 'chart-line-teams-fixed-100') {
            console.log('uplot', options);

            inner_element.innerText = '';
            let new_elem = document.createElement('div');
            new_elem.style.width = '100%';
            new_elem.style.height = '100%';
            new_elem.id = path.replace(/\//gi, '');
            inner_element.appendChild(new_elem);

            requestAnimationFrame(() => {
                console.log('RENDERED');
                console.log('chart-size-rendered?', new_elem.offsetWidth, new_elem.offsetHeight);
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            });

            console.log('chart-size', new_elem.offsetWidth, new_elem.offsetHeight);
            computed_styles = getComputedStyle(new_elem);
            console.log('chart-size-computed', computed_styles.width, computed_styles.height);

            let chart_data = [];
            let chart_series_opts = [];
            let chart_axis_opts = [];

            console.log(options.axis);

            let x_data = data.get(options['x-data']).split(',').map(Number);
            chart_data.push(x_data);
            chart_series_opts.push({});

            ['x', 'y'].forEach((axis) => {
                chart_axis_opts.push({
                    stroke: flatten(options)['axis/' + axis + '/color'] || "#cccccc",
                    grid: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    ticks: {
                        width: 1 / devicePixelRatio,
                        stroke: flatten(options)['axis/' + axis + '/color'] || "#666666",
                    },
                    points: {
                        show: false,
                    },
                    show: varToBoolean(flatten(options)['axis/' + axis + '/show'], true),
                });

                if ((flatten(options)['axis/' + axis + '/type'] || "") === "time") {
                    console.log('axis-type', axis, 'time');
                    chart_axis_opts[chart_axis_opts.length - 1].values = (self, ticks) => ticks.map(rawValue => {
                        minutes = Math.floor(rawValue / 60);
                        seconds = rawValue % 60;

                        return minutes + ':' + String(seconds).padStart(2, "0");
                    });
                }
            });

            options['y-data'].split(',').forEach(function (value, index) {
                index = index + 1;
                let y_data = data.get(value).split(',').map(Number);
                chart_data.push(y_data);

                console.log('y index', index);
                chart_series_opts.push({
                    label: flatten(options)['name/y' + index] || "",
                    width: 1.5,
                    fill: (u, seriesIdx) => {
                        let s = u.series[seriesIdx];
                        let sc = u.scales[s.scale];

                        return scaleGradient(u, s.scale, 1, [
                            [sc.min, data.get(flatten(options)['team-colors/1']) || 'rgba(0, 0, 255, 0.5)'],
                            [0, data.get(flatten(options)['team-colors/1']) || 'rgba(0, 0, 255, 0.5)'],
                            [0, "white"],
                            [0, data.get(flatten(options)['team-colors/0']) || 'rgba(255, 0, 0, 0.5)'],
                            [sc.max, data.get(flatten(options)['team-colors/0']) || 'rgba(255, 0, 0, 0.5)'],
                        ]);
                    },
                    stroke: '#DDDDDD'
                });
            });

            console.log(chart_data);

            chart_config = {
                width: new_elem.offsetWidth,
                height: new_elem.offsetHeight,
                series: chart_series_opts,
                scales: {
                    "x": {
                        time: false,
                    },
                    "y": {
                        auto: false,
                        range: [-100, 100],
                    }
                },
                axes: chart_axis_opts,
            };

            console.log('uplot', chart_config);

            charts[path] = uPlot(chart_config, chart_data, new_elem);

            new ResizeObserver(() => {
                charts[path].setSize({
                    width: new_elem.offsetWidth,
                    height: new_elem.offsetHeight
                });
            }).observe(new_elem);
        } else if (mode === 'scroll-left') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let marquee = document.createElement('marquee');
                marquee.direction = 'left';
                marquee.scrollAmount = 7.5;
                marquee.innerText = data.get(path);

                inner_element.appendChild(marquee);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'qr-code') {
            inner_element.innerText = '';

            new QRCode(inner_element, {
                text: data.get(path),
                width: 512,
                height: 512,
                border: 2
            });
        } else if (mode === 'timer-milliseconds') {
            inner_element.innerText = (Number(data.get(path)) / 1000).toFixed(0);
        } else if (mode === 'number-comma') {
            inner_element.innerText = Number(data.get(path)).toLocaleString("en-US");
        } else if (mode === 'round') {
            inner_element.innerText = Number(data.get(path)).toFixed(2);
        } else if (mode === 'round-0') {
            inner_element.innerText = Number(data.get(path)).toFixed(0);
        } else if (mode === 'round-2') {
            inner_element.innerText = Number(data.get(path)).toFixed(2);
        } else if (mode === 'round-1') {
            inner_element.innerText = Number(data.get(path)).toFixed(1);
        } else if (mode === 'image') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = data.get(path);

                img.onerror = function () {
                    this.style.display = 'none';
                };

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'svg') {
            inner_element.innerText = '';

            let img = document.createElement('img');
            img.src = data.get(path);

            inner_element.appendChild(img);

            SVGInject(img);
        } else if (mode === 'series-score') {
            inner_element.innerHTML = '<i class=\'fas fa-circle\'></i>'.repeat(parseInt(data.get(path), 10));
        } else if (mode === 'icon-font') {
            inner_element.innerHTML = '<i class=\'' + data.get(path) + '\'></i>';
        } else if (mode === 'iconify-icon') {
            //inner_element.innerHTML = '<iconify-icon icon=\'' + data.get(path) + '\'></iconify-icon>';
            const iconify_icon = document.createElement('iconify-icon');

            iconify_icon.loadIcon(data.get(path))
                .then((data) => {
                    console.log(data)
                    let icon_elem = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                    icon_elem.setAttribute('width', '1em');
                    icon_elem.setAttribute('height', '1em');
                    icon_elem.setAttribute('viewBox', '0 0 24 24');

                    icon_elem.setHTMLUnsafe(data.body);

                    inner_element.appendChild(icon_elem);
                })
                .catch(console.error);
        } else if (mode === 'steering') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('i');

                new_elem.id = path;
                new_elem.classList.add('fas');
                new_elem.classList.add('fa-steering-wheel');
                new_elem.style.transition = "all .1s linear";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.transform = 'rotate(' + Number(data.get(path)) * -90 + 'deg)';
        } else if (mode === 'steering-text') {
            inner_element.innerText = (Number(data.get(path)) * -90).toFixed(0) + '°';
        } else if (mode === 'openweathermap-icon') {
            inner_element.innerHTML = '<i class=\'wi wi-fw wi-owm-' + data.get(path) + '\'></i>';
        } else if (mode === 'wmo4680-icon') {
            inner_element.innerHTML = '<i class=\'wi wi-fw wi-wmo4680-' + data.get(path) + '\'></i>';
        } else if (mode === 'temp-f-round') {
            inner_element.innerHTML = Number(data.get(path)).toFixed(0) + ' <i class="wi wi-fahrenheit"></i>';
        } else if (mode === 'temp-c-round') {
            inner_element.innerHTML = Number(data.get(path)).toFixed(0) + ' <i class="wi wi-celsius"></i>';
        } else if (mode === 'temp-f') {
            inner_element.innerText = data.get(path) + ' <i class="wi wi-fahrenheit"></i>';
        } else if (mode === 'temp-c') {
            inner_element.innerText = data.get(path) + ' <i class="wi wi-celsius"></i>';
        } else if (mode === 'mm-to-in-round') {
            inner_element.innerHTML = Number(Number(data.get(path)) * 0.03937).toFixed(2) + ' in.';
        } else if (mode === 'prefix-plus') {
            if (data.get(path) !== '') {
                inner_element.innerHTML = '<i class="fas fa-plus" style="position: absolute; left: 0px; padding-top: var(--sg-pad-top);"></i> ' + data.get(path);
            } else {
                inner_element.innerHTML = '';
            }
        } else if (mode === 'prefix-dollar-sign') {
            inner_element.innerHTML = '<i class="fas fa-dollar-sign"></i> ' + data.get(path);
        } else if (mode === 'prefix-dollar-sign-usd') {
            inner_element.innerHTML = '<i class="fas fa-dollar-sign"></i> ' + Number(data.get(path).replace(',', '')).toFixed(2);
        } else if (mode === 'prefix-dollar-sign-usd-short') {
            inner_element.innerHTML = '<i class="fas fa-dollar-sign"></i> ' + Number(data.get(path).replace(',', '')).toFixed(0);
        } else if (mode === 'postfix-percent-sign') {
            inner_element.innerHTML = data.get(path) + ' %';
        } else if (mode === 'postfix-percent-sign-round') {
            inner_element.innerHTML = Number(data.get(path)).toFixed(0) + ' %';
        } else if (mode === 'postfix-percent-sign-round-color') {
            var num = Number(data.get(path));
            inner_element.innerHTML = num.toFixed(0) + ' %';
            if (num > 0) {
                inner_element.style.color = 'rgb(128, 255, 128)';
            } else if (num < 0) {
                inner_element.style.color = 'rgb(255, 128, 128)';
            } else {
                inner_element.style.color = 'rgb(255, 255, 255)';
            }
        } else if (mode === 'postfix-percent-sign-round-2') {
            inner_element.innerHTML = Number(data.get(path)).toFixed(2) + ' %';
        } else if (mode === 'postfix-percent-sign-round-2-color') {
            let num = Number(data.get(path).replace(',', '').replace('%', ''));
            inner_element.innerHTML = num.toFixed(2) + ' %';
            if (num > 0) {
                inner_element.style.color = 'rgb(128, 255, 128)';
            } else if (num < 0) {
                inner_element.style.color = 'rgb(255, 128, 128)';
            } else {
                inner_element.style.color = 'rgb(255, 255, 255)';
            }
        } else if (mode === 'postfix-percent-100-sign-round') {
            inner_element.innerHTML = (Number(data.get(path)) * 100).toFixed(0) + ' %';
        } else if (mode === 'postfix-percent-100-sign-round-sm') {
            inner_element.innerHTML = (Number(data.get(path)) * 100).toFixed(0) + '%';
        } else if (mode === 'postfix-percent-100-sign-round-2') {
            inner_element.innerHTML = (Number(data.get(path)) * 100).toFixed(2) + ' %';
        } else if (mode === 'postfix-percent-100-sign-round-2-sm') {
            inner_element.innerHTML = (Number(data.get(path)) * 100).toFixed(2) + '%';
        } else if (mode === 'postfix-in') {
            inner_element.innerHTML = (Number(data.get(path))).toFixed(2) + ' \"';
        } else if (mode === 'postfix-hr') {
            inner_element.innerHTML = data.get(path) + ' hr. <i class="fas fa-raindrops"></i>';
        } else if (mode === 'filled-score') {
            if (/^\d+$/.test(data.get(path))) {
                inner_element.innerHTML = '<i class="fas fa-fw fa-square-full" style="height: .5em; transform: scaleX(4) translateY(-1em); margin-top: calc((100vh/27) * .5); margin-left: .6em; margin-right: .6em;"></i>'.repeat(parseInt(data.get(path)));
            } else {
                inner_element.innerText = data.get(path);
            }
        } else if (mode === 'open-score') {
            if (/^\d+$/.test(data.get(path))) {
                inner_element.innerHTML = '<i class="fas fa-fw fa-square-full" style="height: .5em; transform: scaleX(4) translateY(-1em); margin-top: calc((100vh/27) * .5); margin-left: .6em; margin-right: .6em; opacity: .3; color: #e6e6e6;"></i>'.repeat(parseInt(data.get(path)));
            } else {
                inner_element.innerText = data.get(path);
            }
        } else if (mode === 'filled-score-half') {
            if (/^\d+$/.test(data.get(path))) {
                inner_element.innerHTML = '<i class="fas fa-fw fa-square-full" style="height: .5em; transform: scaleX(4) translateY(-1em); margin-left: .6em; margin-right: .6em;"></i>'.repeat(parseInt(data.get(path)));
            } else {
                inner_element.innerText = data.get(path);
            }
        } else if (mode === 'open-score-half') {
            if (/^\d+$/.test(data.get(path))) {
                inner_element.innerHTML = '<i class="fas fa-fw fa-square-full" style="height: .5em; transform: scaleX(4) translateY(-1em); margin-left: .6em; margin-right: .6em; opacity: .3; color: #e6e6e6;"></i>'.repeat(parseInt(data.get(path)));
            } else {
                inner_element.innerText = data.get(path);
            }
        } else if (mode === 'among-us-icon') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = '/static/gaming-icons/among-us/' + data.get(path) + '.png';

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'minecraft-icon') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = '/static/gaming-icons/minecraft/' + data.get(path) + '.png';
                img.style = 'image-rendering: pixelated; height: 75%;';

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'minecraft-status-icon') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = '/static/gaming-icons/minecraft/health-formated/' + data.get(path) + '.png';
                img.style = 'image-rendering: pixelated;';

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'minecraft-inventory-icon') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = '/static/gaming-icons/minecraft/inv/' + data.get(path) + '.png';
                img.style = 'image-rendering: pixelated;';

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'minecraft-effect-icons') {
            if (data.get(path) !== '') {
                inner_element.innerHTML = '';
                let inner_span = '<span style="border-radius: .25em; background-color: #b2b2b2; padding-right: .25em;">';


                data.get(path).split(',').forEach(function (effect) {
                    let image = effect.split('|')[0];
                    let time = effect.split('|')[1];

                    inner_span +=
                        '<img src=\'/static/gaming-icons/minecraft/effects/' + image + '.png\' style=\'image-rendering: pixelated; height: 50%; margin-left: .25em;\'/> ' + time;
                });

                inner_element.innerHTML += inner_span + '</span>';
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'csgo-icon') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = '/static/gaming-icons/csgo/weapon_icons/' + data.get(path) + '.png';

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'hltv-csgo-icon') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = '/static/gaming-icons/csgo/weapon_icons/weapon_' + data.get(path) + '_active.png';

                inner_element.appendChild(img);

            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'audio') {
            if (data.get(path) !== '') {
                inner_element.innerHTML =
                    "<audio autoplay ><source src='" + data.get(path) + "'></audio>";
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'mp4-video' || mode === 'hls-video') {
            if (data.get(path) !== '') {
                if (path.replace(/\//gi, '') in videojs.players) {
                    console.log('FOUND VIDEO WITH ID');
                    let curr_vid = videojs.getPlayer(path.replace(/\//gi, ''));
                    if (curr_vid.currentSource().src !== data.get(path)) {
                        console.log('NEW VIDEO URL - DESTROYING');
                        curr_vid.dispose();
                        delete videojs.players[path.replace(/\//gi, '')];

                        inner_element.innerHTML =
                            "<video loop autoplay muted class='video-js vjs-default-skin' data-setup='{}' style='height: 100% !important; width: auto !important;' id='" + path.replace(/\//gi, '') + "'><source src='" +
                            data.get(path) +
                            "' type='application/x-mpegURL'></video>";

                        videojs(inner_element.querySelector('.video-js'));

                        videojs.getPlayer(path.replace(/\//gi, '')).on('playing', () => {
                            videojs.getPlayer(path.replace(/\//gi, '')).textTracks().tracks_.forEach(track => track.mode = "hidden");
                        });

                        videojs.getPlayer(path.replace(/\//gi, '')).play();
                    } else {
                        console.log('SAME VIDEO URL - NOT TOUCHING');
                        curr_vid.muted(true);
                    }
                } else {
                    console.log('NEW VIDEO');

                    inner_element.innerHTML =
                        "<video loop autoplay muted class='video-js vjs-default-skin' data-setup='{}' style='height: 100% !important; width: auto !important;' id='" + path.replace(/\//gi, '') + "'><source src='" +
                        data.get(path) +
                        "' type='application/x-mpegURL'></video>";

                    videojs(inner_element.querySelector('.video-js'));

                    videojs.getPlayer(path.replace(/\//gi, '')).on('playing', () => {
                        videojs.getPlayer(path.replace(/\//gi, '')).textTracks().tracks_.forEach(track => track.mode = "hidden");
                    });

                    videojs.getPlayer(path.replace(/\//gi, '')).play();
                }

            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'hls-video-unmuted') {
            if (data.get(path) !== '') {
                if (path.replace(/\//gi, '') in videojs.players) {
                    console.log('FOUND VIDEO WITH ID');
                    let curr_vid = videojs.getPlayer(path.replace(/\//gi, ''));
                    if (curr_vid.currentSource().src !== data.get(path)) {
                        console.log('NEW VIDEO URL - DESTROYING');
                        curr_vid.dispose();
                        delete videojs.players[path.replace(/\//gi, '')];

                        inner_element.innerHTML =
                            "<video loop autoplay class='video-js vjs-default-skin' data-setup='{}' style='height: 100% !important; width: auto !important;' id='" + path.replace(/\//gi, '') + "'><source src='" +
                            data.get(path) +
                            "' type='application/x-mpegURL'></video>";

                        videojs(inner_element.querySelector('.video-js'));

                        videojs.getPlayer(path.replace(/\//gi, '')).on('playing', () => {
                            videojs.getPlayer(path.replace(/\//gi, '')).textTracks().tracks_.forEach(track => track.mode = "hidden");
                        });

                        videojs.getPlayer(path.replace(/\//gi, '')).play();
                    } else {
                        console.log('SAME VIDEO URL - NOT TOUCHING');
                        curr_vid.muted(false);
                    }
                } else {
                    console.log('NEW VIDEO');

                    inner_element.innerHTML =
                        "<video loop autoplay class='video-js vjs-default-skin' data-setup='{}' style='height: 100% !important; width: auto !important;' id='" + path.replace(/\//gi, '') + "'><source src='" +
                        data.get(path) +
                        "' type='application/x-mpegURL'></video>";

                    videojs(inner_element.querySelector('.video-js'));

                    videojs.getPlayer(path.replace(/\//gi, '')).on('playing', () => {
                        videojs.getPlayer(path.replace(/\//gi, '')).textTracks().tracks_.forEach(track => track.mode = "hidden");
                    });

                    videojs.getPlayer(path.replace(/\//gi, '')).play();
                }

            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'janus-live') {
            if (path in janusObjects) {
                endJanusStream(path);
            }

            if (data.get(path) !== '') {
                inner_element.innerHTML = "<video muted autoplay style='height: 100% !important; width: auto !important;' id='" + path + "'></video>";
                watchJanusStream(data.get(path), path);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'webcam') {
            if (data.get(path) !== '') {
                startWebcamViaName('OBS-Camera', inner_element, path);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-dragon-icon') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');
                img.src = '/static/gaming-icons/league-of-legends/pngs/' + data.get(path) + '.png';

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-champion') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) in lol_champ_data_corrected) {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/img/champion/' + lol_champ_data_corrected[data.get(path)]["image"]["full"];
                    inner_element.appendChild(img);
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-champion-key') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) === "0") {
                    inner_element.innerText = "";
                } else if (data.get(path) in lol_champ_data_id) {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/img/champion/' + lol_champ_data_id[data.get(path)]["image"]["full"];
                    inner_element.appendChild(img);
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-champion-key-name') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) === "0") {
                    inner_element.innerText = "";
                } else if (data.get(path) in lol_champ_data_id) {
                    inner_element.innerText = lol_champ_data_id[data.get(path)]["name"];
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-champion-id') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) in lol_champ_data) {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/img/champion/' + lol_champ_data[data.get(path)]["image"]["full"];
                    inner_element.appendChild(img);
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-champion-id-name') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) in lol_champ_data) {
                    inner_element.innerText = lol_champ_data[data.get(path)]["name"];
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-champion-loading-id') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) in lol_champ_data) {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/img/champion/loading/' + data.get(path) + '_0.jpg';
                    inner_element.appendChild(img);
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-champion-loading-key') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) === "0") {
                    inner_element.innerText = "";
                } else if (data.get(path) in lol_champ_data_id) {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/img/champion/loading/' + lol_champ_data_id[data.get(path)]['id'] + '_0.jpg';
                    inner_element.appendChild(img);
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-summoner-spell') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                let img = document.createElement('img');

                split_spell_name = data.get(path).split("_");
                corrected_spell_name = split_spell_name[split_spell_name.length - 2];
                if (corrected_spell_name.startsWith("SummonerSmite")) {
                    corrected_spell_name = "SummonerSmite";
                }

                img.src = 'https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/img/spell/' + corrected_spell_name + '.png';

                inner_element.appendChild(img);
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-summoner-spell-key') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) in lol_summoner_spell_id) {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/img/spell/' + lol_summoner_spell_id[data.get(path)]['image']['full'];
                    inner_element.appendChild(img);
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-perk-key') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) in lol_perk_id) {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/img/' + lol_perk_id[data.get(path)]['icon'];
                    inner_element.appendChild(img);
                } else {
                    inner_element.innerText = data.get(path);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'lol-item') {
            if (data.get(path) !== '') {
                inner_element.innerText = '';

                if (data.get(path) !== "0") {
                    let img = document.createElement('img');
                    img.src = 'https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/img/item/' + data.get(path) + '.png';

                    inner_element.appendChild(img);
                }
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'csgo-round-kills-icon') {
            if (parseInt(data.get(path)) >= 1) {
                inner_element.innerHTML = '<span class="fa-layers fa-fw">' +
                    '<i class="fas fa-skull" style="transform: scale(.75) translate(-15%, 15%);"></i>' +
                    '<span class="fa-layers-counter" style="transform: scale(.5); background-color: RGBA(255, 0, 0, .75)">' + data.get(path) + '</span>' +
                    '</span>';
            } else {
                inner_element.innerText = '';
            }
        } else if (mode === 'length-bar-1-extra-life-blue') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "#26C2EB";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = Number(data.get(path)) * 100 + '%';
        } else if (mode === 'length-bar-1-extra-life-green') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "#7FD836";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = Number(data.get(path)) * 100 + '%';
        } else if (mode === 'length-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "black";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'length-bar-100-green') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "green";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'length-bar-100-red') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "#ff8080";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'length-bar-100-blue') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "#8080ff";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'length-bar-1-red') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "#ff8080";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = data.get(path) * 100 + '%';
        } else if (mode === 'length-bar-1-blue') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "#8080ff";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.height = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.width = data.get(path) * 100 + '%';
        } else if (mode === 'height-bar-100-green') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "green";
                new_elem.style.transition = "all 1s linear";
                new_elem.style.width = "100%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.height = data.get(path) + '%';
        } else if (mode === 'height-bar-100-green-bottom') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "green";
                new_elem.style.transition = "all 1s ease";
                new_elem.style.width = "100%";
                new_elem.style.bottom = "0%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.height = data.get(path) + '%';
        } else if (mode === 'height-bar-100-auto-bottom') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "blue";
                new_elem.style.transition = "all 1s ease";
                new_elem.style.width = "100%";
                new_elem.style.bottom = "0%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.height = data.get(path) + '%';

            if (path + '-color' in data) {
                document.getElementById(path).style.backgroundColor = data.get(path + '-color');
            } else {
                document.getElementById(path).style.backgroundColor = "blue";
            }
        } else if (mode === 'height-bar-100-voicemeeter-bottom' || mode === 'height-bar-100-voicemeeter-bottom-volume') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.backgroundColor = "blue";
                new_elem.style.transition = "all .1s ease";
                new_elem.style.width = "100%";
                new_elem.style.bottom = "0%";

                inner_element.appendChild(new_elem);
            }

            document.getElementById(path).style.height = data.get(path) + '%';

            if (data.has(path + "-color")) {
                document.getElementById(path).style.backgroundColor = data.get(path + '-color');
            } else {
                document.getElementById(path).style.backgroundColor = "blue";
            }
        } else if (mode === 'height-bar-100-voicemeeter-bottom-level') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let new_elem = document.createElement('div');

                new_elem.id = path;
                new_elem.classList.add('layer');
                new_elem.style.transition = "all .1s ease";
                new_elem.style.width = "100%";
                new_elem.style.bottom = "0%";

                inner_element.appendChild(new_elem);
            }
            let to_edit = document.getElementById(path);

            // to_edit.style.height = data.get(path) + '%';
            to_edit.style.setProperty('--vol-h', data.get(path) + '%');

            // varToBoolean(data.has(path.split('/').slice(0, -1).join('/') + "/mute")) && 

            if (varToBoolean(data.get(path.split('/').slice(0, -1).join('/') + "/mute"))) {
                to_edit.style.backgroundImage = "linear-gradient(0deg, #444444 0%, #444444 min(var(--vol-h), 100%), #00000000 min(var(--vol-h), 100%))";
            } else {
                to_edit.style.backgroundImage = "linear-gradient(0deg, #68858c 0%, #68858c min(var(--vol-h), 55.55555%), #2dcc37 min(var(--vol-h), 55.55555%), #2dcc37 min(var(--vol-h), 66.66666%), #b8cc2d min(var(--vol-h), 66.66666%), #b8cc2d min(var(--vol-h), 79.16666%), #cc0000 min(var(--vol-h), 79.16666%), #cc0000 min(var(--vol-h), 83.33333%), #ac37fa min(var(--vol-h), 83.33333%), #ac37fa min(var(--vol-h), 100%), #00000000 min(var(--vol-h), 100%))";
            }
        } else if (mode === 'health-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#800000";
                health_elem.style.transition = "all 1s linear";
                health_elem.style.height = "100%";
                health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'csgo-ct-health-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#276186";
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'csgo-t-health-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#947200";
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'csgo-ct-left-health-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#276186";
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.transform = "skewX(10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'csgo-t-left-health-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#947200";
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.transform = "skewX(10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'csgo-ct-right-health-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#276186";
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'csgo-t-right-health-bar-100') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#947200";
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'rl-boost-total') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#00802d";
                //health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                //health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = parseFloat(data.get(path)) + '%';
        } else if (mode === 'rl-boost-total-home') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/light");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                //health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = parseFloat(data.get(path)) + '%';
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/light");
        } else if (mode === 'rl-boost-total-away') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/light");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                //health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = parseFloat(data.get(path)) + '%';
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/light");
        } else if (mode === 'rl-total-opacity-home') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/light");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.width = "100%";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.opacity = parseFloat(data.get(path)) * 0.5 + 0.5;
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/light");
        } else if (mode === 'rl-total-opacity-away') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/light");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.width = "100%";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.opacity = parseFloat(data.get(path)) * 0.5 + 0.5;
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/light");
        } else if (mode === 'rl-boost-total-home-dark') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/dark");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                //health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = parseFloat(data.get(path)) + '%';
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/dark");
        } else if (mode === 'rl-boost-total-away-dark') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/dark");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                //health_elem.style.transform = "skewX(-10deg)";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = parseFloat(data.get(path)) + '%';
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/dark");
        } else if (mode === 'rl-total-opacity-home-dark') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/dark");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.width = "100%";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.opacity = parseFloat(data.get(path)) * 0.5 + 0.5;
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/0/color_primary/dark");
        } else if (mode === 'rl-total-opacity-away-dark') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/dark");
                health_elem.style.transition = "all .25s linear";
                health_elem.style.height = "100%";
                health_elem.style.width = "100%";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.opacity = parseFloat(data.get(path)) * 0.5 + 0.5;
            document.getElementById(path).style.backgroundColor = data.get("data/com/rocketleague/gsi/extra/teams/1/color_primary/dark");
        } else if (mode === 'csgo-ct-health-bar-100-no-animate') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#276186";
                health_elem.style.height = "100%";
                health_elem.style.transition = "all 0.0s linear";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'csgo-t-health-bar-100-no-animate') {
            if (!document.getElementById(path)) {
                inner_element.innerText = '';
                let health_elem = document.createElement('div');

                health_elem.id = path;
                health_elem.classList.add('layer');
                health_elem.style.backgroundColor = "#947200";
                health_elem.style.height = "100%";

                inner_element.appendChild(health_elem);
            }

            document.getElementById(path).style.width = data.get(path) + '%';
        } else if (mode === 'countdown-10') {
            if (!document.getElementById(path)) {
                let countdown_elem = document.createElement('div');

                countdown_elem.id = path;
                inner_element.appendChild(countdown_elem);

                makeCountdown(path, countdown_elem, 10, data.get(path));
                setRemainingPathColor(path, data.get(path), 5, 2.5);
                setCircleDasharray(path, 10, data.get(path));
            } else {
                if (Number(document.getElementById(`${path}-timer-label`).innerText) < data.get(path)) {
                    inner_element.innerHTML = '';

                    let countdown_elem = document.createElement('div');

                    countdown_elem.id = path;
                    inner_element.appendChild(countdown_elem);

                    makeCountdown(path, countdown_elem, 10, data.get(path));
                    setRemainingPathColor(path, data.get(path), 5, 2.5);
                    setCircleDasharray(path, 10, data.get(path));
                } else {
                    document.getElementById(`${path}-timer-label`).innerText = data.get(path);
                    setRemainingPathColor(path, data.get(path), 5, 2.5);
                    setCircleDasharray(path, 10, data.get(path));
                }
            }
        } else {
            console.log('Warning: Unknown mode for ', element.id);
            inner_element.innerText = data.get(path);
        }
    } else {
        inner_element.innerText = data.get(path);
    }
}

var FULL_DASH_ARRAY = 283;

function makeCountdown(path, element_for, time_limit, time_left) {
    // Edited from: https://css-tricks.com/how-to-create-an-animated-countdown-timer-with-html-css-and-javascript/

    var TIME_LIMIT = time_limit;
    let timeLeft = time_left;

    let remainingPathColor = "green";

    remainingPathColor = "green";


    let rawTimeFraction = (timeLeft / TIME_LIMIT);
    rawTimeFraction = rawTimeFraction - (1 / TIME_LIMIT) * (1 - rawTimeFraction);

    var circleDasharray = `${(
        rawTimeFraction * FULL_DASH_ARRAY
    ).toFixed(0)} ${FULL_DASH_ARRAY}`;

    element_for.innerHTML = `
        <div id="${path}-timer" class="base-timer">
            <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" style="height: 90%; width: 90%; margin: 5%;">
                <g class="base-timer__circle">
                <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
                <path
                    id="${path}-timer-path-remaining"
                    stroke-dasharray="${circleDasharray}"
                    class="base-timer__path-remaining"
                    data-sg-timer-remaining-color="${remainingPathColor}"
                    d="
                    M 50, 50
                    m -45, 0
                    a 45,45 0 1,0 90,0
                    a 45,45 0 1,0 -90,0
                    "
                ></path>
                </g>
            </svg>
            <span id="${path}-timer-label" class="base-timer__label">${timeLeft}</span>
        </div>
    `;
}

function setRemainingPathColor(path, time_left, warning, alert) {

    if (time_left <= alert) {
        document.getElementById(`${path}-timer-path-remaining`).setAttribute('data-sg-timer-remaining-color', 'red');
    } else if (time_left <= warning) {
        document.getElementById(`${path}-timer-path-remaining`).setAttribute('data-sg-timer-remaining-color', 'orange');
    } else {
        document.getElementById(`${path}-timer-path-remaining`).setAttribute('data-sg-timer-remaining-color', 'green');
    }
}

function calculateTimeFraction(time_limit, time_left) {
    var rawTimeFraction = time_left / time_limit;
    return rawTimeFraction - (1 / time_limit) * (1 - rawTimeFraction);
}

function setCircleDasharray(path, time_limit, time_left) {
    var circleDasharray = `${(
        calculateTimeFraction(time_limit, time_left) * FULL_DASH_ARRAY
    ).toFixed(0)} 283`;
    document.getElementById(`${path}-timer-path-remaining`).setAttribute("stroke-dasharray", circleDasharray);
}