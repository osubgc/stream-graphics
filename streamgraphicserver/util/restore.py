import configparser
import glob
import json
import logging
import os
import pprint
import threading
import time

from jinja2 import Environment, select_autoescape
from jinja2.loaders import ChoiceLoader, FileSystemLoader
from ruamel.yaml import YAML
from semantic_version import SimpleSpec, Version
from streamgraphicserver.util.data import get_item_jinja, send_data_multiple
from streamgraphicserver.util.other import flatten

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

yaml = YAML(typ="safe")

JINJA_ENV = Environment(
    loader=ChoiceLoader([FileSystemLoader(".")]),
    autoescape=select_autoescape([]),
)
JINJA_ENV.globals.update(get_data_value=get_item_jinja)


RESTORE_LIST = []


def run_script(restore_filename, script_name=None, variables={}):
    logger.info("Restoring (w/ thread): %s %s %s", restore_filename, script_name, variables)
    data = restore_file(restore_filename, variables)

    if script_name:
        thread = threading.Thread(target=run_next_script, kwargs={"data": data, "script_name": script_name})
        thread.start()
    else:
        run_init(data)


def run_script_no_thread(restore_filename, script_name=None, variables={}):
    logger.info("Restoring: %s %s %s", restore_filename, script_name, variables)
    data = restore_file(restore_filename, variables)

    if script_name:
        run_next_script(data, script_name)
    else:
        run_init(data)


def restore_file(restore_filename, variables={}):
    (data, _) = render_templated_restore(restore_filename + ".yaml.j2", variables=variables)

    if "header" in data:
        if "version" in data["header"]:
            if Version(data["header"]["version"]) not in SimpleSpec(">=14.0.0,<15.0.0"):
                logger.error("ERROR: Restore file version unsupported.")
        if "dependencies" in data["header"]:
            for dependency in data["header"]["dependencies"]:
                if "script" in dependency:
                    run_script_no_thread(
                        "restore/" + dependency["filename"],
                        dependency["script"],
                    )
                else:
                    run_script_no_thread("restore/" + dependency["filename"], None)
    return data


def run_init(data):
    if "init" in data:
        send_data_multiple(flatten(data["init"]))


def run_next_script(data, script_name):
    logger.info("----- Starting Script: %s -----", script_name)
    logger.info(pprint.pformat(data["scripts"].keys()))
    if "dependency" in data["scripts"][script_name]:
        dependency = data["scripts"][script_name]["dependency"]

        logger.info("--- Running Dependency: %s %s ---", dependency["file"], dependency["script"])

        run_script("restore/" + dependency["file"] + ".yaml", dependency["script"])
    for script_line in data["scripts"][script_name]["timeline"]:
        if "time_after" in script_line:
            time.sleep(script_line["time_after"])
        else:
            time.sleep(1)

        logger.info(pprint.pformat(script_line))
        if "changes" in script_line:
            send_data_multiple(flatten(script_line["changes"]))


def list_restore_files():
    template_restore = list(glob.iglob("restore/**/overlays/**/*.yaml.j2", recursive=True))

    return template_restore


def list_restore_file_names():
    template_restore = [y.replace(".yaml.j2", "") for y in list_restore_files()]

    return template_restore


def get_restore_properties_cache():
    global RESTORE_LIST

    if len(RESTORE_LIST) > 0:
        return RESTORE_LIST
    else:
        list_restore_properties()
        return RESTORE_LIST


def get_restore_properties_cache_filtered(filter_str=""):
    global RESTORE_LIST

    restore_list_filtered = []

    if not len(RESTORE_LIST) > 0:
        list_restore_properties()

    for restore in RESTORE_LIST:
        if restore["name"].startswith(filter_str):
            restore_list_filtered.append(restore)

    return restore_list_filtered


def render_templated_restore(restore_filename, variables={}):
    print("attempting to open:", restore_filename)

    cfg_name = restore_filename[: -len(".yaml.j2")] + ".cfg"
    short_name = "/".join(restore_filename.split(os.path.sep)[1:])[: -len(".yaml.j2")]
    restore_template = JINJA_ENV.get_template(restore_filename)

    if os.path.exists(cfg_name):
        with open(cfg_name, "r", encoding="utf-8") as cfg_file:
            config = configparser.ConfigParser()
            config.read_file(cfg_file)

            jinja_vars = dict(config["DEFAULT"])
    else:
        jinja_vars = {}

    with open("config.json", "r", encoding="utf-8") as json_data:
        curr_config = json.load(json_data)

        for k, v in curr_config.items():
            if k.startswith("restore_vars/" + short_name + "/"):
                jinja_vars[k.replace("restore_vars/" + short_name + "/", "")] = v

    jinja_vars.update(variables)

    new_config = {**curr_config, **flatten(jinja_vars, parent_key="restore_vars/" + short_name)}

    with open("config.json", "w", encoding="utf-8") as json_data_new:
        json.dump(new_config, json_data_new, indent=4)

    return yaml.load(restore_template.render(**jinja_vars)), jinja_vars


def list_restore_properties():
    global RESTORE_LIST

    restore_list_tmp = []

    for restore_filename in list_restore_files():
        tic = time.perf_counter()
        jinja_vars = []

        short_name = "/".join(restore_filename.split(os.path.sep)[1:])[: -len(".yaml.j2")]
        data, jinja_vars = render_templated_restore(restore_filename)

        toc = time.perf_counter()

        print("Read", short_name, f"in {toc - tic:0.4f} seconds")

        if "header" in data:
            restore_list_tmp.append(
                {
                    "name": short_name,
                    "file": restore_filename,
                    "supported": False,
                    "has_init": False,
                    "scripts": [],
                    "vars": jinja_vars,
                }
            )

            if "version" in data["header"]:
                if Version(data["header"]["version"]) in SimpleSpec(">=14.0.0,<15.0.0"):
                    restore_list_tmp[-1]["supported"] = True
                    if "init" in data:
                        restore_list_tmp[-1]["has_init"] = True

                    if "scripts" in data:
                        for script in data["scripts"]:
                            restore_list_tmp[-1]["scripts"].append({"name": script, "timeline": [], "total_time": 0})
                            if "timeline" in data["scripts"][script]:
                                for timeline_item in data["scripts"][script]["timeline"]:
                                    if "time_after" in timeline_item:
                                        time_after = timeline_item["time_after"]
                                        restore_list_tmp[-1]["scripts"][-1]["timeline"].append(time_after)
                                        restore_list_tmp[-1]["scripts"][-1]["total_time"] += time_after

    RESTORE_LIST = restore_list_tmp


if __name__ == "__main__":
    logger.info(pprint.pformat(list_restore_file_names()))
    logger.info(pprint.pformat(list_restore_properties()))
