ALLOWED_EXTENSIONS = ["png", "jpg", "jpeg", "gif", "mp3", "wav", "svg"]


def get_static_files():
    import glob

    files = []
    for file_ext in ALLOWED_EXTENSIONS:
        files.extend(glob.glob("streamgraphicserver/static/local/**/*." + file_ext, recursive=True))

    files_link = map(lambda f: f.replace("streamgraphicserver", ""), files)

    return sorted(list(files_link))
