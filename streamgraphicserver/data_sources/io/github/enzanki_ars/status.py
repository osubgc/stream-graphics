import logging
import multiprocessing
import sched
import time
import gevent

import requests
from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

API_PATH_STATUS = "data/io/github/enzanki-ars/status"

REQUESTS_SESSION = requests.Session()

STATUS_SUMMARIES = {
    "unknown": {
        "icon": "fas fa-question",
        "bg": "#222222",
    },
    "info": {
        "icon": "fas fa-info",
        "bg": "#660066",
    },
    "green": {
        "icon": "fas fa-check",
        "bg": "#006600",
    },
    "yellow": {
        "icon": "fas fa-exclamation",
        "bg": "#666600",
    },
    "orange": {
        "icon": "fas fa-times",
        "bg": "#662600",
    },
    "red": {
        "icon": "fas fa-power-off",
        "bg": "#660000",
    },
}

STATUS_FEEDS = {
    "statuspage": {
        "icon": "/api/v1/icons/simpleicons/statuspage?fill=FFFFFF",
        "data_path": "status.atlassian.com",
        "data_type": "statuspage",
    },
    "confluence": {
        "icon": "/api/v1/icons/simpleicons/confluence?fill=FFFFFF",
        "data_path": "confluence.status.atlassian.com",
        "data_type": "statuspage",
    },
    "fontawesome": {
        "icon": "/api/v1/icons/simpleicons/fontawesome?fill=FFFFFF",
        "data_path": "status.fortawesome.com",
        "data_type": "statuspage",
    },
    "python": {
        "icon": "/api/v1/icons/simpleicons/python?fill=FFFFFF",
        "data_path": "status.python.org",
        "data_type": "statuspage",
    },
    "canvas": {
        "icon": "/static/local/canvas.svg",
        "data_path": "status.instructure.com",
        "data_type": "statuspage",
    },
    "cloudflare": {
        "icon": "/api/v1/icons/simpleicons/cloudflare?fill=FFFFFF",
        "data_path": "www.cloudflarestatus.com",
        "data_type": "statuspage",
    },
    "fastly": {
        "icon": "/api/v1/icons/simpleicons/fastly?fill=FFFFFF",
        "data_path": "status.fastly.com",
        "data_type": "statuspage",
    },
    "epicgames": {
        "icon": "/api/v1/icons/simpleicons/epicgames?fill=FFFFFF",
        "data_path": "status.epicgames.com",
        "data_type": "statuspage",
    },
    "github": {
        "icon": "/api/v1/icons/simpleicons/github?fill=FFFFFF",
        "data_path": "www.githubstatus.com",
        "data_type": "statuspage",
    },
    "twitter": {
        "icon": "/api/v1/icons/simpleicons/twitter?fill=FFFFFF",
        "data_path": "api.twitterstat.us",
        "data_type": "statuspage",
    },
    "discord": {
        "icon": "/api/v1/icons/simpleicons/discord?fill=FFFFFF",
        "data_path": "discordstatus.com",
        "data_type": "statuspage",
    },
    "npm": {
        "icon": "/api/v1/icons/simpleicons/npm?fill=FFFFFF",
        "data_path": "status.npmjs.org",
        "data_type": "statuspage",
    },
    "twitch": {
        "icon": "/api/v1/icons/simpleicons/twitch?fill=FFFFFF",
        "data_path": "status.twitch.tv",
        "data_type": "statuspage",
    },
    "zoom": {
        "icon": "/api/v1/icons/simpleicons/zoom?fill=FFFFFF",
        "data_path": "status.zoom.us",
        "data_type": "statuspage",
    },
    "akamai": {
        "icon": "/static/local/akamai.svg",
        "data_path": "www.akamaistatus.com",
        "data_type": "statuspage",
    },
    "digitalocean": {
        "icon": "/api/v1/icons/simpleicons/digitalocean?fill=FFFFFF",
        "data_path": "status.digitalocean.com",
        "data_type": "statuspage",
    },
    "duo": {
        "icon": "/static/local/duo.svg",
        "data_path": "status.duo.com",
        "data_type": "statuspage",
    },
    "coinbase": {
        "icon": "/api/v1/icons/simpleicons/coinbase?fill=FFFFFF",
        "data_path": "status.coinbase.com",
        "data_type": "statuspage",
    },
    "coinbase_pro": {
        "icon": "/api/v1/icons/simpleicons/coinbase?fill=FFFFFF",
        "data_path": "status.pro.coinbase.com",
        "data_type": "statuspage",
    },
    "esea": {
        "icon": "/api/v1/icons/simpleicons/esea?fill=FFFFFF",
        "data_path": "status.esea.net",
        "data_type": "statuspage",
    },
    "faceit": {
        "icon": "/api/v1/icons/simpleicons/faceit?fill=FFFFFF",
        "data_path": "www.faceitstatus.com",
        "data_type": "statuspage",
    },
    "redhat": {
        "icon": "/api/v1/icons/simpleicons/redhat?fill=FFFFFF",
        "data_path": "status.redhat.com",
        "data_type": "statuspage",
    },
}


def run_get_status():
    logger = logging.getLogger(__name__)

    for status_name, status_info in STATUS_FEEDS.items():
        status_summary = "unknown"

        logger.info("Fetching status for %s", status_name)

        if status_info["data_type"] == "statuspage":
            status_data = REQUESTS_SESSION.get("https://" + status_info["data_path"] + "/api/v2/summary.json").json()

            if status_data["status"]["indicator"] == "none":
                status_summary = "green"
            elif status_data["status"]["indicator"] == "minor":
                status_summary = "yellow"
            elif status_data["status"]["indicator"] == "major":
                status_summary = "orange"
            elif status_data["status"]["indicator"] == "critical":
                status_summary = "red"

        status_info["status"] = STATUS_SUMMARIES[status_summary]
        status_info["status"]["bg_css"] = "background-color: " + status_info["status"]["bg"]

    status_keys = sorted(list(STATUS_FEEDS.keys()))

    send_data_multiple(
        flatten(
            {**STATUS_FEEDS, "entries": ",".join(status_keys)},
            parent_key=API_PATH_STATUS,
        )
    )


def run_get_status_loop(scheduler):
    logger = logging.getLogger(__name__)

    logger.info("Running new loop for io/github/enzanki_ars/status.")

    run_get_status()

    scheduler.enter(120, 1, run_get_status_loop, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting io/github/enzanki_ars/status data_source.")

    scheduler = sched.scheduler(time.time, gevent.sleep)
    scheduler.enter(0, 1, run_get_status_loop, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    print("This script is not designed to be run independently.")
