from streamgraphicserver.util.remote_data import send_data, send_data_multiple
from streamgraphicserver.util.other import get_path_from_dict, flatten
from datetime import datetime

previous_phase = "live"
bomb_start_time = datetime.now()
bomb_timer_start = 40.0
bomb_timer_real_string = ""
defuse_string = ""

CSGO_WIN_TIME_ICONS = {
    "bomb": "fa-solid fa-bomb",
    "elimination": "fa-solid fa-skull",
    "time": "fa-solid fa-stopwatch",
    "defuse": "fa-solid fa-scissors",
}


def clamp_int(n, min_i, max_i):
    if not (isinstance(n, int) or isinstance(n, float)):
        return 0

    if n < min_i:
        return min_i
    elif n > max_i:
        return max_i
    else:
        return n


def safe_percent(left, right):
    total = left + right

    return left / total if total else 1


def safe_percent_100(left, right):
    return safe_percent(left, right) * 100


def parse_csgo_data(data):
    global previous_phase
    global bomb_start_time
    global bomb_timer_real_string
    global bomb_timer_start
    global defuse_string

    if get_path_from_dict(data, ["map", "team_ct", "name"]) == "":
        ct_name = "Counter-Terrorists"
    else:
        ct_name = get_path_from_dict(data, ["map", "team_ct", "name"])

    if get_path_from_dict(data, ["map", "team_t", "name"]) == "":
        t_name = "Terrorists"
    else:
        t_name = get_path_from_dict(data, ["map", "team_t", "name"])

    if get_path_from_dict(data, ["map", "round"]) != "":
        round_num = get_path_from_dict(data, ["map", "round"]) + 1
    else:
        round_num = ""

    countdown_raw = get_path_from_dict(data, ["phase_countdowns", "phase_ends_in"])
    if countdown_raw != "":
        countdown = int(float(countdown_raw)) + 1
        countdown_minutes, countdown_seconds = divmod(countdown, 60)
        countdown_string = "{:01d}:{:02d}".format(countdown_minutes, countdown_seconds)
    else:
        countdown = ""
        countdown_minutes = ""
        countdown_seconds = ""
        countdown_string = ""

    phase = get_path_from_dict(data, ["phase_countdowns", "phase"])

    phase_component = "component/csgo-gsi-timer/style/bg"

    bomb_state = get_path_from_dict(data, ["bomb", "state"])

    if phase == "live":
        send_data(phase_component, "dark-grey")
    elif phase == "defuse":
        send_data(phase_component, "dark-green")
        defuse_string = countdown_raw

        delta = datetime.now() - bomb_start_time
        bomb_timer_real = 40.0 - (delta.seconds + delta.microseconds * 0.000001)
        bomb_timer_real_string = "{:01.1f}".format(bomb_timer_real)
        send_data("layer/csgo-gsi-bomb-countdown/display", "show")
    elif phase == "bomb":
        send_data(phase_component, "dark-red")
        if previous_phase != "bomb" and previous_phase != "defuse":
            bomb_start_time = datetime.now()
            bomb_timer_start = float(countdown_raw)
            bomb_timer_real = bomb_timer_start
        else:
            delta = datetime.now() - bomb_start_time
            bomb_timer_real = bomb_timer_start - (delta.seconds + (delta.microseconds * 0.000001))
            bomb_timer_real_string = "{:01.1f}".format(bomb_timer_real)
            send_data("layer/csgo-gsi-bomb-countdown/display", "show")
    elif phase == "freezetime":
        send_data(phase_component, "dark-yellow")
        send_data("layer/csgo-gsi-bomb-countdown/display", "fade-out")
        bomb_timer_real_string = ""
        defuse_string = ""
    elif phase == "over":
        send_data(phase_component, "dark-blue")
        # send_data("layer/csgo-gsi-bomb-countdown/display", "fade-out")
        # bomb_timer_real_string = ""
        # defuse_string = ""
    else:
        send_data(phase_component, "dark-blue")
        send_data("layer/csgo-gsi-bomb-countdown/display", "fade-out")
        bomb_timer_real_string = ""
        defuse_string = ""

    previous_phase = phase

    parsed_data = {
        "spec_target": {
            "name": get_path_from_dict(data, ["player", "name"]),
            "observer_slot": get_path_from_dict(data, ["player", "observer_slot"]),
            "match_stats": {
                "kills": get_path_from_dict(data, ["player", "match_stats", "kills"]),
                "deaths": get_path_from_dict(data, ["player", "match_stats", "deaths"]),
                "assists": get_path_from_dict(data, ["player", "match_stats", "assists"]),
                "mvps": get_path_from_dict(data, ["player", "match_stats", "mvps"]),
                "score": get_path_from_dict(data, ["player", "match_stats", "score"]),
            },
            "state": {
                "armor": get_path_from_dict(data, ["player", "state", "armor"]),
                "burning": get_path_from_dict(data, ["player", "state", "burning"]),
                "defusekit": get_path_from_dict(data, ["player", "state", "defusekit"]),
                "equip_value": get_path_from_dict(data, ["player", "state", "equip_value"]),
                "flashed": get_path_from_dict(data, ["player", "state", "flashed"]),
                "health": get_path_from_dict(data, ["player", "state", "health"]),
                "helmet": get_path_from_dict(data, ["player", "state", "helmet"]),
                "money": get_path_from_dict(data, ["player", "state", "money"]),
                "round_killhs": get_path_from_dict(data, ["player", "state", "round_killhs"]),
                "round_kills": get_path_from_dict(data, ["player", "state", "round_kills"]),
                "round_totaldmg": get_path_from_dict(data, ["player", "state", "round_totaldmg"]),
                "smoked": get_path_from_dict(data, ["player", "state", "smoked"]),
            },
            "team": get_path_from_dict(data, ["player", "team"]),
            "weapons": {
                "grenade_0": {"name": "", "state": "", "graphic": ""},
                "grenade_1": {"name": "", "state": "", "graphic": ""},
                "grenade_2": {"name": "", "state": "", "graphic": ""},
                "grenade_3": {"name": "", "state": "", "graphic": ""},
                "bomb": {"name": "", "state": "", "graphic": ""},
                "knife": {"name": "", "state": "", "graphic": ""},
                "pistol": {
                    "name": "",
                    "type": "",
                    "state": "",
                    "ammo_clip": "",
                    "ammo_clip_max": "",
                    "ammo_reserve": "",
                    "graphic": "",
                },
                "rifle": {
                    "name": "",
                    "type": "",
                    "state": "",
                    "ammo_clip": "",
                    "ammo_clip_max": "",
                    "ammo_reserve": "",
                    "graphic": "",
                },
                "slot0": {
                    "name": get_path_from_dict(data, ["player", "weapons", "weapon_0", "name"]),
                    "paintkit": get_path_from_dict(data, ["player", "weapons", "weapon_0", "paintkit"]),
                    "state": get_path_from_dict(data, ["player", "weapons", "weapon_0", "state"]),
                    "type": get_path_from_dict(data, ["player", "weapons", "weapon_0", "type"]),
                    "ammo_clip": get_path_from_dict(data, ["player", "weapons", "weapon_1", "ammo_clip"]),
                    "ammo_clip_max": get_path_from_dict(data, ["player", "weapons", "weapon_1", "ammo_clip_max"]),
                    "ammo_reserve": get_path_from_dict(data, ["player", "weapons", "weapon_1", "ammo_reserve"]),
                    "graphic": get_path_from_dict(data, ["player", "weapons", "weapon_0", "name"])
                    + "_"
                    + get_path_from_dict(data, ["player", "weapons", "weapon_0", "state"]).lower(),
                },
                "slot1": {
                    "name": get_path_from_dict(data, ["player", "weapons", "weapon_1", "name"]),
                    "paintkit": get_path_from_dict(data, ["player", "weapons", "weapon_1", "paintkit"]),
                    "state": get_path_from_dict(data, ["player", "weapons", "weapon_1", "state"]),
                    "type": get_path_from_dict(data, ["player", "weapons", "weapon_1", "type"]),
                    "ammo_clip": get_path_from_dict(data, ["player", "weapons", "weapon_1", "ammo_clip"]),
                    "ammo_clip_max": get_path_from_dict(data, ["player", "weapons", "weapon_1", "ammo_clip_max"]),
                    "ammo_reserve": get_path_from_dict(data, ["player", "weapons", "weapon_1", "ammo_reserve"]),
                    "graphic": get_path_from_dict(data, ["player", "weapons", "weapon_1", "name"])
                    + "_"
                    + get_path_from_dict(data, ["player", "weapons", "weapon_1", "state"]).lower(),
                },
                "slot2": {
                    "name": get_path_from_dict(data, ["player", "weapons", "weapon_2", "name"]),
                    "paintkit": get_path_from_dict(data, ["player", "weapons", "weapon_2", "paintkit"]),
                    "state": get_path_from_dict(data, ["player", "weapons", "weapon_2", "state"]),
                    "type": get_path_from_dict(data, ["player", "weapons", "weapon_2", "type"]),
                    "ammo_clip": get_path_from_dict(data, ["player", "weapons", "weapon_2", "ammo_clip"]),
                    "ammo_clip_max": get_path_from_dict(data, ["player", "weapons", "weapon_2", "ammo_clip_max"]),
                    "ammo_reserve": get_path_from_dict(data, ["player", "weapons", "weapon_2", "ammo_reserve"]),
                    "graphic": get_path_from_dict(data, ["player", "weapons", "weapon_2", "name"])
                    + "_"
                    + get_path_from_dict(data, ["player", "weapons", "weapon_2", "state"]).lower(),
                },
                "slot3": {
                    "name": get_path_from_dict(data, ["player", "weapons", "weapon_3", "name"]),
                    "paintkit": get_path_from_dict(data, ["player", "weapons", "weapon_3", "paintkit"]),
                    "state": get_path_from_dict(data, ["player", "weapons", "weapon_3", "state"]),
                    "type": get_path_from_dict(data, ["player", "weapons", "weapon_3", "type"]),
                    "ammo_clip": get_path_from_dict(data, ["player", "weapons", "weapon_3", "ammo_clip"]),
                    "ammo_clip_max": get_path_from_dict(data, ["player", "weapons", "weapon_3", "ammo_clip_max"]),
                    "ammo_reserve": get_path_from_dict(data, ["player", "weapons", "weapon_3", "ammo_reserve"]),
                    "graphic": get_path_from_dict(data, ["player", "weapons", "weapon_3", "name"])
                    + "_"
                    + get_path_from_dict(data, ["player", "weapons", "weapon_3", "state"]).lower(),
                },
                "slot4": {
                    "name": get_path_from_dict(data, ["player", "weapons", "weapon_4", "name"]),
                    "paintkit": get_path_from_dict(data, ["player", "weapons", "weapon_4", "paintkit"]),
                    "state": get_path_from_dict(data, ["player", "weapons", "weapon_4", "state"]),
                    "type": get_path_from_dict(data, ["player", "weapons", "weapon_4", "type"]),
                    "ammo_clip": get_path_from_dict(data, ["player", "weapons", "weapon_4", "ammo_clip"]),
                    "ammo_clip_max": get_path_from_dict(data, ["player", "weapons", "weapon_4", "ammo_clip_max"]),
                    "ammo_reserve": get_path_from_dict(data, ["player", "weapons", "weapon_4", "ammo_reserve"]),
                    "graphic": get_path_from_dict(data, ["player", "weapons", "weapon_4", "name"])
                    + "_"
                    + get_path_from_dict(data, ["player", "weapons", "weapon_4", "state"]).lower(),
                },
                "slot5": {
                    "name": get_path_from_dict(data, ["player", "weapons", "weapon_5", "name"]),
                    "paintkit": get_path_from_dict(data, ["player", "weapons", "weapon_5", "paintkit"]),
                    "state": get_path_from_dict(data, ["player", "weapons", "weapon_5", "state"]),
                    "type": get_path_from_dict(data, ["player", "weapons", "weapon_5", "type"]),
                    "ammo_clip": get_path_from_dict(data, ["player", "weapons", "weapon_5", "ammo_clip"]),
                    "ammo_clip_max": get_path_from_dict(data, ["player", "weapons", "weapon_5", "ammo_clip_max"]),
                    "ammo_reserve": get_path_from_dict(data, ["player", "weapons", "weapon_5", "ammo_reserve"]),
                    "graphic": get_path_from_dict(data, ["player", "weapons", "weapon_5", "name"])
                    + "_"
                    + get_path_from_dict(data, ["player", "weapons", "weapon_5", "state"]).lower(),
                },
                "slot6": {
                    "name": get_path_from_dict(data, ["player", "weapons", "weapon_6", "name"]),
                    "paintkit": get_path_from_dict(data, ["player", "weapons", "weapon_6", "paintkit"]),
                    "state": get_path_from_dict(data, ["player", "weapons", "weapon_6", "state"]),
                    "type": get_path_from_dict(data, ["player", "weapons", "weapon_6", "type"]),
                    "ammo_clip": get_path_from_dict(data, ["player", "weapons", "weapon_6", "ammo_clip"]),
                    "ammo_clip_max": get_path_from_dict(data, ["player", "weapons", "weapon_6", "ammo_clip_max"]),
                    "ammo_reserve": get_path_from_dict(data, ["player", "weapons", "weapon_6", "ammo_reserve"]),
                    "graphic": get_path_from_dict(data, ["player", "weapons", "weapon_6", "name"])
                    + "_"
                    + get_path_from_dict(data, ["player", "weapons", "weapon_6", "state"]).lower(),
                },
            },
        },
        "phase_countdowns": {
            "phase": phase,
            "phase_ends_in": countdown_string,
            "phase_ends_in_raw": countdown_raw,
        },
        "map": {
            "teams": {
                "team_ct": {
                    "consecutive_round_losses": get_path_from_dict(data, ["map", "team_ct", "consecutive_round_losses"]),
                    "loss_bonus": 1400
                    + (
                        clamp_int(
                            get_path_from_dict(data, ["map", "team_ct", "consecutive_round_losses"]),
                            0,
                            4,
                        )
                        * 500
                    ),
                    "economy": 0,
                    "equipment_value": 0,
                    "flag": get_path_from_dict(data, ["map", "team_ct", "flag"]),
                    "matches_won_this_series": get_path_from_dict(data, ["map", "team_ct", "matches_won_this_series"]),
                    "name": ct_name,
                    "score": get_path_from_dict(data, ["map", "team_ct", "score"]),
                    "timeouts_remaining": get_path_from_dict(data, ["map", "team_ct", "timeouts_remaining"]),
                },
                "team_t": {
                    "consecutive_round_losses": get_path_from_dict(data, ["map", "team_t", "consecutive_round_losses"]),
                    "loss_bonus": 1400
                    + (
                        clamp_int(
                            get_path_from_dict(data, ["map", "team_t", "consecutive_round_losses"]),
                            0,
                            4,
                        )
                        * 500
                    ),
                    "economy": 0,
                    "equipment_value": 0,
                    "flag": get_path_from_dict(data, ["map", "team_t", "flag"]),
                    "matches_won_this_series": get_path_from_dict(data, ["map", "team_t", "matches_won_this_series"]),
                    "name": t_name,
                    "score": get_path_from_dict(data, ["map", "team_t", "score"]),
                    "timeouts_remaining": get_path_from_dict(data, ["map", "team_t", "timeouts_remaining"]),
                },
            },
            "team_sides": {
                "team_left": {
                    "consecutive_round_losses": "",
                    "loss_bonus": "",
                    "economy": "",
                    "equipment_value": "",
                    "flag": "",
                    "matches_won_this_series": "",
                    "name": "",
                    "score": "",
                    "timeouts_remaining": "",
                    "players_remaining": 0,
                },
                "team_right": {
                    "consecutive_round_losses": "",
                    "loss_bonus": "",
                    "economy": "",
                    "equipment_value": "",
                    "flag": "",
                    "matches_won_this_series": "",
                    "name": "",
                    "score": "",
                    "timeouts_remaining": "",
                    "players_remaining": 0,
                },
            },
            "info": {
                "current_spectators": get_path_from_dict(data, ["map", "current_spectators"]),
                "mode": get_path_from_dict(data, ["map", "mode"]),
                "name": get_path_from_dict(data, ["map", "name"]),
                "num_matches_to_win_series": get_path_from_dict(data, ["map", "num_matches_to_win_series"]),
                "phase": get_path_from_dict(data, ["map", "phase"]),
                "round": round_num,
                "souvenirs_total": get_path_from_dict(data, ["map", "souvenirs_total"]),
            },
            "round_wins_raw": get_path_from_dict(data, ["map", "round_wins"]),
            "round_wins": {},
        },
        "bomb": {
            "countdown": get_path_from_dict(data, ["bomb", "countdown"]),
            "real": bomb_timer_real_string,
            "defuse": defuse_string,
            "state": bomb_state,
        },
        "round": {
            "bomb": get_path_from_dict(data, ["round", "bomb"]),
            "phase": get_path_from_dict(data, ["round", "phase"]),
            "win_team": get_path_from_dict(data, ["round", "win_team"]),
        },
        "players": {
            "slot_"
            + str(i): {
                "name": "",
                "observer_slot": "",
                "match_stats": {
                    "kills": "",
                    "deaths": "",
                    "assists": "",
                    "mvps": "",
                    "score": "",
                },
                "state": {
                    "armor": "",
                    "burning": "",
                    "defusekit": "",
                    "equip_value": "",
                    "flashed": "",
                    "health": 0,
                    "helmet": "",
                    "money": "",
                    "round_killhs": "",
                    "round_kills": "",
                    "round_totaldmg": "",
                    "smoked": "",
                },
                "team": "",
                "weapons": {
                    "grenade_0": {"name": "", "type": "", "graphic": ""},
                    "grenade_1": {"name": "", "type": "", "graphic": ""},
                    "grenade_2": {"name": "", "type": ""},
                    "grenade_3": {"name": "", "type": "", "graphic": ""},
                    "pistol": {
                        "name": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                    },
                    "rifle": {
                        "name": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                    "slot0": {
                        "name": "",
                        "paintkit": "",
                        "state": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                    "slot1": {
                        "name": "",
                        "paintkit": "",
                        "state": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                    "slot2": {
                        "name": "",
                        "paintkit": "",
                        "state": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                    "slot3": {
                        "name": "",
                        "paintkit": "",
                        "state": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                    "slot4": {
                        "name": "",
                        "paintkit": "",
                        "state": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                    "slot5": {
                        "name": "",
                        "paintkit": "",
                        "state": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                    "slot6": {
                        "name": "",
                        "paintkit": "",
                        "state": "",
                        "type": "",
                        "ammo_clip": "",
                        "ammo_clip_max": "",
                        "ammo_reserve": "",
                        "graphic": "",
                    },
                },
            }
            for i in range(0, 10)
        },
    }

    team_left_money = 0
    team_left_equip = 0
    team_right_money = 0
    team_right_equip = 0

    team_util = {
        "team_left": {
            "hegrenade": 0,
            "incgrenade": 0,
            "flashbang": 0,
            "smokegrenade": 0,
        },
        "team_right": {
            "hegrenade": 0,
            "incgrenade": 0,
            "flashbang": 0,
            "smokegrenade": 0,
        },
    }

    alive = {
        "team_left": 0,
        "team_right": 0,
    }

    # fix observer slots
    correct_obs_slot = {}
    obs_slot_info_max = {"ct": 0, "t": 0}
    obs_slot_info_count = {"ct": 0, "t": 0}

    for player in get_path_from_dict(data, ["allplayers"]):

        player_slot = int(get_path_from_dict(data, ["allplayers", str(player), "observer_slot"]))
        player_team = str(get_path_from_dict(data, ["allplayers", str(player), "team"])).lower()

        # Make logic easier...
        if player_slot == 0:
            player_slot = 10

        obs_slot_info_max[player_team] = max(player_slot, obs_slot_info_max[player_team])
        obs_slot_info_count[player_team] += 1

    right_side = "ct" if (obs_slot_info_max["ct"] > obs_slot_info_max["t"]) else "t"
    left_side = "t" if (obs_slot_info_max["ct"] > obs_slot_info_max["t"]) else "ct"

    right_start = 5 - obs_slot_info_count[left_side]

    for player in get_path_from_dict(data, ["allplayers"]):
        player_slot = get_path_from_dict(data, ["allplayers", str(player), "observer_slot"])
        player_team = str(get_path_from_dict(data, ["allplayers", str(player), "team"])).lower()

        new_player_slot = player_slot

        if player_team == right_side:
            new_player_slot = player_slot + right_start

        correct_obs_slot[player_slot] = new_player_slot

    # all players
    for player in get_path_from_dict(data, ["allplayers"]):

        player_slot = get_path_from_dict(data, ["allplayers", str(player), "observer_slot"])
        player_slot = correct_obs_slot[player_slot]

        if player_slot >= 1 and player_slot <= 5:
            player_side = "team_left"
        else:
            player_side = "team_right"

        if get_path_from_dict(data, ["allplayers", str(player), "state", "health"]) > 0:
            alive[player_side] += 1
            parsed_data["map"]["team_sides"][player_side]["players_remaining"] = alive[player_side]

        if get_path_from_dict(data, ["allplayers", str(player), "observer_slot"]) == 1:
            econ_bar_component = "component/csgo-gsi-team-economy-compare-fg/mode"
            econ_equip_bar_component = "component/csgo-gsi-team-equipment-compare-fg/mode"

            health_bar_style = "csgo-" + str(get_path_from_dict(data, ["allplayers", str(player), "team"])).lower() + "-health-bar-100"

            send_data(econ_bar_component, health_bar_style)
            send_data(econ_equip_bar_component, health_bar_style)

            if get_path_from_dict(data, ["allplayers", str(player), "team"]) == "CT":
                parsed_data["map"]["team_sides"] = {
                    "team_left": {
                        "consecutive_round_losses": get_path_from_dict(data, ["map", "team_ct", "consecutive_round_losses"]),
                        "loss_bonus": 1400
                        + (
                            clamp_int(
                                get_path_from_dict(data, ["map", "team_ct", "consecutive_round_losses"]),
                                0,
                                4,
                            )
                            * 500
                        ),
                        "economy": 0,
                        "equipment_value": 0,
                        "flag": get_path_from_dict(data, ["map", "team_ct", "flag"]),
                        "matches_won_this_series": get_path_from_dict(data, ["map", "team_ct", "matches_won_this_series"]),
                        "name": ct_name,
                        "color": "background-color: #276186; color: #FFFFFF",
                        "color_dark": "background-color: #183c53; color: #FFFFFF",
                        "color_text": "color: #3d98d4;",
                        "score": get_path_from_dict(data, ["map", "team_ct", "score"]),
                        "timeouts_remaining": get_path_from_dict(data, ["map", "team_ct", "timeouts_remaining"]),
                        "players_remaining": alive["team_left"],
                    },
                    "team_right": {
                        "consecutive_round_losses": get_path_from_dict(data, ["map", "team_t", "consecutive_round_losses"]),
                        "loss_bonus": 1400
                        + (
                            clamp_int(
                                get_path_from_dict(data, ["map", "team_t", "consecutive_round_losses"]),
                                0,
                                4,
                            )
                            * 500
                        ),
                        "economy": 0,
                        "equipment_value": 0,
                        "flag": get_path_from_dict(data, ["map", "team_t", "flag"]),
                        "matches_won_this_series": get_path_from_dict(data, ["map", "team_t", "matches_won_this_series"]),
                        "name": t_name,
                        "color": "background-color: #947200; color: #FFFFFF",
                        "color_dark": "background-color: #61511a; color: #FFFFFF",
                        "color_text": "color: #e0ad00;",
                        "score": get_path_from_dict(data, ["map", "team_t", "score"]),
                        "timeouts_remaining": get_path_from_dict(data, ["map", "team_t", "timeouts_remaining"]),
                        "players_remaining": alive["team_right"],
                    },
                }
            elif get_path_from_dict(data, ["allplayers", str(player), "team"]) == "T":
                parsed_data["map"]["team_sides"] = {
                    "team_left": {
                        "consecutive_round_losses": get_path_from_dict(data, ["map", "team_t", "consecutive_round_losses"]),
                        "loss_bonus": 1400
                        + (
                            clamp_int(
                                get_path_from_dict(data, ["map", "team_t", "consecutive_round_losses"]),
                                0,
                                4,
                            )
                            * 500
                        ),
                        "economy": 0,
                        "equipment_value": 0,
                        "flag": get_path_from_dict(data, ["map", "team_t", "flag"]),
                        "matches_won_this_series": get_path_from_dict(data, ["map", "team_t", "matches_won_this_series"]),
                        "name": t_name,
                        "color": "background-color: #947200; color: #FFFFFF",
                        "color_dark": "background-color: #61511a; color: #FFFFFF",
                        "color_text": "color: #e0ad00;",
                        "score": get_path_from_dict(data, ["map", "team_t", "score"]),
                        "timeouts_remaining": get_path_from_dict(data, ["map", "team_t", "timeouts_remaining"]),
                        "players_remaining": alive["team_left"],
                    },
                    "team_right": {
                        "consecutive_round_losses": get_path_from_dict(data, ["map", "team_ct", "consecutive_round_losses"]),
                        "loss_bonus": 1400
                        + (
                            clamp_int(
                                get_path_from_dict(data, ["map", "team_ct", "consecutive_round_losses"]),
                                0,
                                4,
                            )
                            * 500
                        ),
                        "economy": 0,
                        "equipment_value": 0,
                        "flag": get_path_from_dict(data, ["map", "team_ct", "flag"]),
                        "matches_won_this_series": get_path_from_dict(data, ["map", "team_ct", "matches_won_this_series"]),
                        "name": ct_name,
                        "color": "background-color: #276186; color: #FFFFFF",
                        "color_dark": "background-color: #183c53; color: #FFFFFF",
                        "color_text": "color: #3d98d4;",
                        "score": get_path_from_dict(data, ["map", "team_ct", "score"]),
                        "timeouts_remaining": get_path_from_dict(data, ["map", "team_ct", "timeouts_remaining"]),
                        "players_remaining": alive["team_right"],
                    },
                }

        player_equip_value = get_path_from_dict(data, ["allplayers", str(player), "state", "equip_value"])
        player_money = get_path_from_dict(data, ["allplayers", str(player), "state", "money"])
        slot_name = "slot_" + str(player_slot)

        health_bar_component = "component/csgo-gsi-slot-" + str(player_slot) + "-health-bar/mode"

        if player_slot >= 1 and player_slot <= 5:
            team_left_equip += player_equip_value
            team_left_money += player_money

            health_bar_style = "csgo-" + str(get_path_from_dict(data, ["allplayers", str(player), "team"])).lower() + "-left-health-bar-100"
        else:
            team_right_equip += player_equip_value
            team_right_money += player_money

            health_bar_style = "csgo-" + str(get_path_from_dict(data, ["allplayers", str(player), "team"])).lower() + "-right-health-bar-100"

        send_data(health_bar_component, health_bar_style)

        observing_component = "component/csgo-gsi-slot-" + str(get_path_from_dict(data, ["allplayers", str(player), "observer_slot"])) + "-slot/style/display"

        if get_path_from_dict(data, ["player", "observer_slot"]) == get_path_from_dict(data, ["allplayers", str(player), "observer_slot"]):
            send_data(observing_component, "show")
        else:
            send_data(observing_component, "hide")

        parsed_data["players"][slot_name] = {
            "name": get_path_from_dict(data, ["allplayers", str(player), "name"]),
            "observer_slot": get_path_from_dict(data, ["allplayers", str(player), "observer_slot"]),
            "match_stats": {
                "kills": get_path_from_dict(data, ["allplayers", str(player), "match_stats", "kills"]),
                "deaths": get_path_from_dict(data, ["allplayers", str(player), "match_stats", "deaths"]),
                "assists": get_path_from_dict(data, ["allplayers", str(player), "match_stats", "assists"]),
                "mvps": get_path_from_dict(data, ["allplayers", str(player), "match_stats", "mvps"]),
                "score": get_path_from_dict(data, ["allplayers", str(player), "match_stats", "score"]),
            },
            "state": {
                "armor": get_path_from_dict(data, ["allplayers", str(player), "state", "armor"]),
                "burning": get_path_from_dict(data, ["allplayers", str(player), "state", "burning"]),
                "defusekit": get_path_from_dict(data, ["allplayers", str(player), "state", "defusekit"]),
                "equip_value": player_equip_value,
                "money": player_money,
                "flashed": get_path_from_dict(data, ["allplayers", str(player), "state", "flashed"]),
                "health": get_path_from_dict(data, ["allplayers", str(player), "state", "health"]),
                "helmet": get_path_from_dict(data, ["allplayers", str(player), "state", "helmet"]),
                "round_killhs": get_path_from_dict(data, ["allplayers", str(player), "state", "round_killhs"]),
                "round_kills": get_path_from_dict(data, ["allplayers", str(player), "state", "round_kills"]),
                "round_totaldmg": get_path_from_dict(data, ["allplayers", str(player), "state", "round_totaldmg"]),
                "smoked": get_path_from_dict(data, ["allplayers", str(player), "state", "smoked"]),
            },
            "team": get_path_from_dict(data, ["allplayers", str(player), "team"]),
            "weapons": {
                "grenade_0": {"name": "", "state": "", "graphic": ""},
                "grenade_1": {"name": "", "state": "", "graphic": ""},
                "grenade_2": {"name": "", "state": "", "graphic": ""},
                "grenade_3": {"name": "", "state": "", "graphic": ""},
                "bomb": {"name": "", "state": "", "graphic": ""},
                "armor": {"graphic": ""},
                "defuse": {"graphic": ""},
                "knife": {"name": "", "state": "", "graphic": ""},
                "pistol": {
                    "name": "",
                    "type": "",
                    "ammo_clip": "",
                    "ammo_clip_max": "",
                    "ammo_reserve": "",
                    "graphic": "",
                },
                "rifle": {
                    "name": "",
                    "type": "",
                    "ammo_clip": "",
                    "ammo_clip_max": "",
                    "ammo_reserve": "",
                    "graphic": "",
                },
                "slot0": {
                    "name": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_0", "name"]),
                    "paintkit": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_0", "paintkit"],
                    ),
                    "state": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_0", "state"],
                    ),
                    "type": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_0", "type"]),
                    "ammo_clip": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_0", "ammo_clip"],
                    ),
                    "ammo_clip_max": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_0",
                            "ammo_clip_max",
                        ],
                    ),
                    "ammo_reserve": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_0",
                            "ammo_reserve",
                        ],
                    ),
                    "graphic": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_0", "name"])
                    + get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_0", "state"],
                    ).lower(),
                },
                "slot1": {
                    "name": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_1", "name"]),
                    "paintkit": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_1", "paintkit"],
                    ),
                    "state": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_1", "state"],
                    ),
                    "type": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_1", "type"]),
                    "ammo_clip": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_1", "ammo_clip"],
                    ),
                    "ammo_clip_max": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_1",
                            "ammo_clip_max",
                        ],
                    ),
                    "ammo_reserve": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_1",
                            "ammo_reserve",
                        ],
                    ),
                    "graphic": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_1", "name"])
                    + get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_1", "state"],
                    ).lower(),
                },
                "slot2": {
                    "name": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_2", "name"]),
                    "paintkit": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_2", "paintkit"],
                    ),
                    "state": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_2", "state"],
                    ),
                    "type": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_2", "type"]),
                    "ammo_clip": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_2", "ammo_clip"],
                    ),
                    "ammo_clip_max": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_2",
                            "ammo_clip_max",
                        ],
                    ),
                    "ammo_reserve": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_2",
                            "ammo_reserve",
                        ],
                    ),
                    "graphic": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_2", "name"])
                    + get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_2", "state"],
                    ).lower(),
                },
                "slot3": {
                    "name": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_3", "name"]),
                    "paintkit": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_3", "paintkit"],
                    ),
                    "state": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_3", "state"],
                    ),
                    "type": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_3", "type"]),
                    "ammo_clip": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_3", "ammo_clip"],
                    ),
                    "ammo_clip_max": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_3",
                            "ammo_clip_max",
                        ],
                    ),
                    "ammo_reserve": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_3",
                            "ammo_reserve",
                        ],
                    ),
                    "graphic": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_3", "name"])
                    + get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_3", "state"],
                    ).lower(),
                },
                "slot4": {
                    "name": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_4", "name"]),
                    "paintkit": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_4", "paintkit"],
                    ),
                    "state": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_4", "state"],
                    ),
                    "type": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_4", "type"]),
                    "ammo_clip": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_4", "ammo_clip"],
                    ),
                    "ammo_clip_max": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_4",
                            "ammo_clip_max",
                        ],
                    ),
                    "ammo_reserve": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_4",
                            "ammo_reserve",
                        ],
                    ),
                    "graphic": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_4", "name"])
                    + get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_4", "state"],
                    ).lower(),
                },
                "slot5": {
                    "name": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_5", "name"]),
                    "paintkit": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_5", "paintkit"],
                    ),
                    "state": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_5", "state"],
                    ),
                    "type": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_5", "type"]),
                    "ammo_clip": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_5", "ammo_clip"],
                    ),
                    "ammo_clip_max": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_5",
                            "ammo_clip_max",
                        ],
                    ),
                    "ammo_reserve": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_5",
                            "ammo_reserve",
                        ],
                    ),
                    "graphic": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_5", "name"])
                    + get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_5", "state"],
                    ).lower(),
                },
                "slot6": {
                    "name": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_6", "name"]),
                    "paintkit": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_6", "paintkit"],
                    ),
                    "state": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_6", "state"],
                    ),
                    "type": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_6", "type"]),
                    "ammo_clip": get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_6", "ammo_clip"],
                    ),
                    "ammo_clip_max": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_6",
                            "ammo_clip_max",
                        ],
                    ),
                    "ammo_reserve": get_path_from_dict(
                        data,
                        [
                            "allplayers",
                            str(player),
                            "weapons",
                            "weapon_6",
                            "ammo_reserve",
                        ],
                    ),
                    "graphic": get_path_from_dict(data, ["allplayers", str(player), "weapons", "weapon_6", "name"])
                    + get_path_from_dict(
                        data,
                        ["allplayers", str(player), "weapons", "weapon_6", "state"],
                    ).lower(),
                },
            },
        }

        if parsed_data["players"][slot_name]["state"]["defusekit"]:
            parsed_data["players"][slot_name]["weapons"]["defuse"] = {"graphic": "weapon_defuser_active"}
        if parsed_data["players"][slot_name]["state"]["armor"]:
            if parsed_data["players"][slot_name]["state"]["helmet"]:
                parsed_data["players"][slot_name]["weapons"]["armor"] = {"graphic": "weapon_armor_helmet_active"}
            else:
                parsed_data["players"][slot_name]["weapons"]["armor"] = {"graphic": "weapon_armor_active"}

        num_grenade = 0

        for weapon_slot in parsed_data["players"][slot_name]["weapons"].keys():
            if weapon_slot.startswith("slot"):
                if parsed_data["players"][slot_name]["weapons"][weapon_slot]["type"] == "":
                    pass

                elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["type"] == "Grenade":
                    parsed_data["players"][slot_name]["weapons"]["grenade_" + str(num_grenade)]["name"] = parsed_data["players"][slot_name]["weapons"][
                        weapon_slot
                    ]["name"]
                    parsed_data["players"][slot_name]["weapons"]["grenade_" + str(num_grenade)]["state"] = parsed_data["players"][slot_name]["weapons"][
                        weapon_slot
                    ]["state"]
                    parsed_data["players"][slot_name]["weapons"]["grenade_" + str(num_grenade)]["graphic"] = (
                        parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                        + "_"
                        + parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"].lower()
                    )

                    if player_side:
                        if parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_hegrenade":

                            team_util[player_side]["hegrenade"] += 1

                        elif (
                            parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_incgrenade"
                            or parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_molotov"
                        ):

                            team_util[player_side]["incgrenade"] += 1

                        elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_flashbang":

                            team_util[player_side]["flashbang"] += 1

                        elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_smokegrenade":

                            team_util[player_side]["smokegrenade"] += 1

                    num_grenade += 1

                    if (
                        "ammo_reserve" in parsed_data["players"][slot_name]["weapons"][weapon_slot]
                        and parsed_data["players"][slot_name]["weapons"][weapon_slot]["ammo_reserve"] > 1
                    ):
                        for i in range(parsed_data["players"][slot_name]["weapons"][weapon_slot]["ammo_reserve"] - 1):
                            parsed_data["players"][slot_name]["weapons"]["grenade_" + str(num_grenade)]["name"] = parsed_data["players"][slot_name]["weapons"][
                                weapon_slot
                            ]["name"]
                            parsed_data["players"][slot_name]["weapons"]["grenade_" + str(num_grenade)]["state"] = parsed_data["players"][slot_name][
                                "weapons"
                            ][weapon_slot]["state"]
                            parsed_data["players"][slot_name]["weapons"]["grenade_" + str(num_grenade)]["graphic"] = (
                                parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                                + "_"
                                + parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"].lower()
                            )

                            if player_side:
                                if parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_hegrenade":

                                    team_util[player_side]["hegrenade"] += 1

                                elif (
                                    parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_incgrenade"
                                    or parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_molotov"
                                ):

                                    team_util[player_side]["incgrenade"] += 1

                                elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_flashbang":

                                    team_util[player_side]["flashbang"] += 1

                                elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"] == "weapon_smokegrenade":

                                    team_util[player_side]["smokegrenade"] += 1

                            num_grenade += 1

                elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["type"] == "Knife":
                    parsed_data["players"][slot_name]["weapons"]["knife"]["name"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                    parsed_data["players"][slot_name]["weapons"]["knife"]["state"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"]
                    parsed_data["players"][slot_name]["weapons"]["knife"]["graphic"] = (
                        parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                        + "_"
                        + parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"].lower()
                    )

                elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["type"] == "C4":
                    parsed_data["players"][slot_name]["weapons"]["bomb"]["name"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                    parsed_data["players"][slot_name]["weapons"]["bomb"]["state"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"]
                    parsed_data["players"][slot_name]["weapons"]["bomb"]["graphic"] = (
                        parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                        + "_"
                        + parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"].lower()
                    )

                elif parsed_data["players"][slot_name]["weapons"][weapon_slot]["type"] == "Pistol":
                    parsed_data["players"][slot_name]["weapons"]["pistol"]["name"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                    parsed_data["players"][slot_name]["weapons"]["pistol"]["state"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"]
                    parsed_data["players"][slot_name]["weapons"]["pistol"]["ammo_clip"] = parsed_data["players"][slot_name]["weapons"][weapon_slot][
                        "ammo_clip"
                    ]
                    parsed_data["players"][slot_name]["weapons"]["pistol"]["ammo_clip_max"] = parsed_data["players"][slot_name]["weapons"][weapon_slot][
                        "ammo_clip_max"
                    ]
                    parsed_data["players"][slot_name]["weapons"]["pistol"]["ammo_reserve"] = parsed_data["players"][slot_name]["weapons"][weapon_slot][
                        "ammo_reserve"
                    ]
                    parsed_data["players"][slot_name]["weapons"]["pistol"]["graphic"] = (
                        parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                        + "_"
                        + parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"].lower()
                    )

                else:
                    parsed_data["players"][slot_name]["weapons"]["rifle"]["name"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                    parsed_data["players"][slot_name]["weapons"]["rifle"]["state"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"]
                    parsed_data["players"][slot_name]["weapons"]["rifle"]["ammo_clip"] = parsed_data["players"][slot_name]["weapons"][weapon_slot]["ammo_clip"]
                    parsed_data["players"][slot_name]["weapons"]["rifle"]["ammo_clip_max"] = parsed_data["players"][slot_name]["weapons"][weapon_slot][
                        "ammo_clip_max"
                    ]
                    parsed_data["players"][slot_name]["weapons"]["rifle"]["ammo_reserve"] = parsed_data["players"][slot_name]["weapons"][weapon_slot][
                        "ammo_reserve"
                    ]
                    parsed_data["players"][slot_name]["weapons"]["rifle"]["graphic"] = (
                        parsed_data["players"][slot_name]["weapons"][weapon_slot]["name"]
                        + "_"
                        + parsed_data["players"][slot_name]["weapons"][weapon_slot]["state"].lower()
                    )

        parsed_data["players"][slot_name]["weapons"].pop("slot0")
        parsed_data["players"][slot_name]["weapons"].pop("slot1")
        parsed_data["players"][slot_name]["weapons"].pop("slot2")
        parsed_data["players"][slot_name]["weapons"].pop("slot3")
        parsed_data["players"][slot_name]["weapons"].pop("slot4")
        parsed_data["players"][slot_name]["weapons"].pop("slot5")
        parsed_data["players"][slot_name]["weapons"].pop("slot6")

    round_win_info = {i: {"sep": "", "ct_icon": "", "t_icon": "", "left_icon": "", "right_icon": ""} for i in range(1, 31)}

    completed_rounds = len(get_path_from_dict(data, ["map", "round_wins"], default={}))

    for round_num, round_win_condition in get_path_from_dict(data, ["map", "round_wins"], default={}).items():
        round_num = int(round_num)

        win_team = round_win_condition.split("_")[0]
        win_type = round_win_condition.split("_")[2]
        loss_team = "t" if win_team == "ct" else "ct"

        win_side = ""
        win_color = ""
        loss_color = ""

        if completed_rounds <= 15:
            if win_team == left_side:
                win_side = "left"
                win_color = parsed_data["map"]["team_sides"]["team_left"]["color_text"]
                loss_color = parsed_data["map"]["team_sides"]["team_right"]["color_text"]
            else:
                win_side = "right"
                win_color = parsed_data["map"]["team_sides"]["team_right"]["color_text"]
                loss_color = parsed_data["map"]["team_sides"]["team_left"]["color_text"]
        elif round_num <= 15:  # Side switched, so switch colors.
            if win_team == left_side:
                win_side = "right"
                win_color = parsed_data["map"]["team_sides"]["team_left"]["color_text"]
                loss_color = parsed_data["map"]["team_sides"]["team_right"]["color_text"]
            else:
                win_side = "left"
                win_color = parsed_data["map"]["team_sides"]["team_right"]["color_text"]
                loss_color = parsed_data["map"]["team_sides"]["team_left"]["color_text"]
        else:
            if win_team == left_side:
                win_side = "left"
                win_color = parsed_data["map"]["team_sides"]["team_left"]["color_text"]
                loss_color = parsed_data["map"]["team_sides"]["team_right"]["color_text"]
            else:
                win_side = "right"
                win_color = parsed_data["map"]["team_sides"]["team_right"]["color_text"]
                loss_color = parsed_data["map"]["team_sides"]["team_left"]["color_text"]

        loss_side = "left" if win_side == "right" else "right"

        round_win_info[round_num] = {
            "sep": "fas fa-horizontal-rule",
            win_team + "_icon": CSGO_WIN_TIME_ICONS[win_type],
            loss_team + "_icon": "",
            win_side + "_icon": CSGO_WIN_TIME_ICONS[win_type],
            win_side + "_color": win_color,
            loss_side + "_icon": "",
            loss_side + "_color": loss_color,
        }

    parsed_data["map"]["round_wins"] = round_win_info

    parsed_data["map"]["team_sides"]["team_left"]["economy"] = team_left_money
    parsed_data["map"]["team_sides"]["team_left"]["equipment_value"] = team_left_equip
    parsed_data["map"]["team_sides"]["team_right"]["economy"] = team_right_money
    parsed_data["map"]["team_sides"]["team_right"]["equipment_value"] = team_right_equip

    parsed_data["map"]["team_sides"]["team_left"]["economy_percent"] = safe_percent_100(team_left_money, team_right_money)
    parsed_data["map"]["team_sides"]["team_left"]["equipment_value_percent"] = safe_percent_100(team_left_equip, team_right_equip)
    parsed_data["map"]["team_sides"]["team_right"]["economy_percent"] = safe_percent_100(team_right_money, team_left_money)
    parsed_data["map"]["team_sides"]["team_right"]["equipment_value_percent"] = safe_percent_100(team_right_equip, team_left_equip)

    health_bar_component = "component/csgo-gsi-current-player-health-bar/mode"

    health_bar_style = "csgo-" + parsed_data["spec_target"]["team"].lower() + "-health-bar-100-no-animate"

    send_data(health_bar_component, health_bar_style)

    for side in team_util:
        team_util[side]["sum"] = team_util[side]["hegrenade"] + team_util[side]["incgrenade"] + team_util[side]["flashbang"] + team_util[side]["smokegrenade"]

        if team_util[side]["sum"] >= 12:
            team_util[side]["text"] = "High"
            team_util[side]["color_primary/css"] = "background-color: rgb(0, 128, 0);"
        elif team_util[side]["sum"] >= 5:
            team_util[side]["text"] = "Medium"
            team_util[side]["color_primary/css"] = "background-color: rgb(64, 128, 0);"
        elif team_util[side]["sum"] >= 1:
            team_util[side]["text"] = "Low"
            team_util[side]["color_primary/css"] = "background-color: rgb(128, 64, 0);"
        else:
            team_util[side]["text"] = "None"
            team_util[side]["color_primary/css"] = "background-color:  rgb(128, 0, 0);"

    send_data_multiple(
        flatten(
            team_util,
            parent_key="data/net/counter-strike/game-state/map/team_sides/utility",
        )
    )
    num_grenade = 0

    for weapon_slot in parsed_data["spec_target"]["weapons"].keys():
        if weapon_slot.startswith("slot"):

            if parsed_data["spec_target"]["weapons"][weapon_slot]["type"] == "":
                pass

            elif parsed_data["spec_target"]["weapons"][weapon_slot]["type"] == "Grenade":
                parsed_data["spec_target"]["weapons"]["grenade_" + str(num_grenade)]["name"] = parsed_data["spec_target"]["weapons"][weapon_slot]["name"]
                parsed_data["spec_target"]["weapons"]["grenade_" + str(num_grenade)]["state"] = parsed_data["spec_target"]["weapons"][weapon_slot]["state"]
                parsed_data["spec_target"]["weapons"]["grenade_" + str(num_grenade)]["graphic"] = (
                    parsed_data["spec_target"]["weapons"][weapon_slot]["name"] + "_" + parsed_data["spec_target"]["weapons"][weapon_slot]["state"].lower()
                )

                num_grenade += 1

                if (
                    "ammo_reserve" in parsed_data["spec_target"]["weapons"][weapon_slot]
                    and parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_reserve"] > 1
                ):
                    for i in range(parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_reserve"] - 1):
                        parsed_data["spec_target"]["weapons"]["grenade_" + str(num_grenade)]["name"] = parsed_data["spec_target"]["weapons"][weapon_slot][
                            "name"
                        ]
                        parsed_data["spec_target"]["weapons"]["grenade_" + str(num_grenade)]["state"] = parsed_data["spec_target"]["weapons"][weapon_slot][
                            "state"
                        ]
                        parsed_data["spec_target"]["weapons"]["grenade_" + str(num_grenade)]["graphic"] = (
                            parsed_data["spec_target"]["weapons"][weapon_slot]["name"]
                            + "_"
                            + parsed_data["spec_target"]["weapons"][weapon_slot]["state"].lower()
                        )

                        num_grenade += 1

            elif parsed_data["spec_target"]["weapons"][weapon_slot]["type"] == "Knife":
                parsed_data["spec_target"]["weapons"]["knife"]["name"] = parsed_data["spec_target"]["weapons"][weapon_slot]["name"]
                parsed_data["spec_target"]["weapons"]["knife"]["state"] = parsed_data["spec_target"]["weapons"][weapon_slot]["state"]
                parsed_data["spec_target"]["weapons"]["knife"]["graphic"] = (
                    parsed_data["spec_target"]["weapons"][weapon_slot]["name"] + "_" + parsed_data["spec_target"]["weapons"][weapon_slot]["state"].lower()
                )

            elif parsed_data["spec_target"]["weapons"][weapon_slot]["type"] == "C4":
                parsed_data["spec_target"]["weapons"]["bomb"]["name"] = parsed_data["spec_target"]["weapons"][weapon_slot]["name"]
                parsed_data["spec_target"]["weapons"]["bomb"]["state"] = parsed_data["spec_target"]["weapons"][weapon_slot]["state"]
                parsed_data["spec_target"]["weapons"]["bomb"]["graphic"] = (
                    parsed_data["spec_target"]["weapons"][weapon_slot]["name"] + "_" + parsed_data["spec_target"]["weapons"][weapon_slot]["state"].lower()
                )

            elif parsed_data["spec_target"]["weapons"][weapon_slot]["type"] == "Pistol":
                parsed_data["spec_target"]["weapons"]["pistol"]["name"] = parsed_data["spec_target"]["weapons"][weapon_slot]["name"]
                parsed_data["spec_target"]["weapons"]["pistol"]["state"] = parsed_data["spec_target"]["weapons"][weapon_slot]["state"]
                parsed_data["spec_target"]["weapons"]["pistol"]["ammo_clip"] = parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_clip"]
                parsed_data["spec_target"]["weapons"]["pistol"]["ammo_clip_max"] = parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_clip_max"]
                parsed_data["spec_target"]["weapons"]["pistol"]["ammo_reserve"] = parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_reserve"]
                parsed_data["spec_target"]["weapons"]["pistol"]["graphic"] = (
                    parsed_data["spec_target"]["weapons"][weapon_slot]["name"] + "_" + parsed_data["spec_target"]["weapons"][weapon_slot]["state"].lower()
                )

            else:
                parsed_data["spec_target"]["weapons"]["rifle"]["name"] = parsed_data["spec_target"]["weapons"][weapon_slot]["name"]
                parsed_data["spec_target"]["weapons"]["rifle"]["state"] = parsed_data["spec_target"]["weapons"][weapon_slot]["state"]
                parsed_data["spec_target"]["weapons"]["rifle"]["ammo_clip"] = parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_clip"]
                parsed_data["spec_target"]["weapons"]["rifle"]["ammo_clip_max"] = parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_clip_max"]
                parsed_data["spec_target"]["weapons"]["rifle"]["ammo_reserve"] = parsed_data["spec_target"]["weapons"][weapon_slot]["ammo_reserve"]
                parsed_data["spec_target"]["weapons"]["rifle"]["graphic"] = (
                    parsed_data["spec_target"]["weapons"][weapon_slot]["name"] + "_" + parsed_data["spec_target"]["weapons"][weapon_slot]["state"].lower()
                )

    if parsed_data["spec_target"]["weapons"]["rifle"]["state"] == "active":
        parsed_data["spec_target"]["state"]["ammo_clip"] = parsed_data["spec_target"]["weapons"]["rifle"]["ammo_clip"]
        parsed_data["spec_target"]["state"]["ammo_clip_max"] = parsed_data["spec_target"]["weapons"]["rifle"]["ammo_clip_max"]
        parsed_data["spec_target"]["state"]["ammo_reserve"] = parsed_data["spec_target"]["weapons"]["rifle"]["ammo_reserve"]
    elif parsed_data["spec_target"]["weapons"]["pistol"]["state"] == "active":
        parsed_data["spec_target"]["state"]["ammo_clip"] = parsed_data["spec_target"]["weapons"]["pistol"]["ammo_clip"]
        parsed_data["spec_target"]["state"]["ammo_clip_max"] = parsed_data["spec_target"]["weapons"]["pistol"]["ammo_clip_max"]
        parsed_data["spec_target"]["state"]["ammo_reserve"] = parsed_data["spec_target"]["weapons"]["pistol"]["ammo_reserve"]
    else:
        parsed_data["spec_target"]["state"]["ammo_clip"] = ""
        parsed_data["spec_target"]["state"]["ammo_clip_max"] = ""
        parsed_data["spec_target"]["state"]["ammo_reserve"] = ""

    parsed_data["spec_target"]["weapons"].pop("slot0")
    parsed_data["spec_target"]["weapons"].pop("slot1")
    parsed_data["spec_target"]["weapons"].pop("slot2")
    parsed_data["spec_target"]["weapons"].pop("slot3")
    parsed_data["spec_target"]["weapons"].pop("slot4")
    parsed_data["spec_target"]["weapons"].pop("slot5")
    parsed_data["spec_target"]["weapons"].pop("slot6")

    return parsed_data
