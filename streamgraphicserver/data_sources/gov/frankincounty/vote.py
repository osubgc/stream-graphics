import datetime
import time
import logging
import multiprocessing
import sched

import pytz
from streamgraphicserver.util.logging import setup_loggers
import requests

from streamgraphicserver.util.remote_data import delete_keys, send_data, send_data_multiple
from streamgraphicserver.util.other import flatten

SESSION = requests.Session()
ENABLE_AREA_DATA = False


def div_ignore_0(n, d):
    return (n / d) * 100 if d else 0


def update_franklincounty_votes(scheduler=None):
    logger = logging.getLogger(__name__)

    logger.info("Refreshing election info for Franklin County.")

    live_results = SESSION.get("https://vote.franklincountyohio.gov/elections/api/election/live").json()
    precincts = SESSION.get("https://vote.franklincountyohio.gov/elections/api/precinct").json()["precincts"]

    for area in precincts:
        del area["poly"]

    vote_name = live_results["name"]
    turnout_data = {}
    voting_data = {}
    total_turnout = {"reg": 0, "abs": 0, "day": 0, "prov": 0, "total": 0, "reporting": 0, "areas": len(live_results["turnout"])}

    for turnout_area in live_results["turnout"]:
        turnout_data[turnout_area["area"]] = turnout_area
        turnout_data[turnout_area["area"]]["total"] = turnout_area["abs"] + turnout_area["day"] + turnout_area["prov"]

        total_turnout["reg"] += turnout_area["reg"]
        total_turnout["abs"] += turnout_area["abs"]
        total_turnout["day"] += turnout_area["day"]
        total_turnout["prov"] += turnout_area["prov"]
        total_turnout["total"] += turnout_area["total"]

        if turnout_area["day"] > 0:
            total_turnout["reporting"] += 1

    total_turnout["percent"] = div_ignore_0(total_turnout["reporting"], total_turnout["areas"])

    for race in live_results["races"]:
        voting_data[race["id"]] = {
            "name": race["name"],
            "choices": [],
            "reg": 0,
            "votes": 0,
            "abs": 0,
            "day": 0,
            "prov": 0,
        }

        for choice in race["choices"]:
            choice_data = {
                "name": choice["name"].replace("Dem", "[D]").replace("Rep", "[R]"),
                "name_short": choice["name"].split(" ")[0],
                "party": choice["party"],
                "overlapVotes": choice["overlapVotes"],
                "overlapAbs": choice["overlapAbs"],
                "overlapProv": choice["overlapProv"],
                "votes": sum(choice["votes"]),
                "abs": sum(choice["abs"]),
                "day": sum(choice["votes"]) - sum(choice["abs"]),
                "prov": sum(choice["prov"]),
            }

            if ENABLE_AREA_DATA:
                choice_data["areas"] = {}

            voting_data[race["id"]]["votes"] += sum(choice["votes"])
            voting_data[race["id"]]["abs"] += sum(choice["abs"])
            voting_data[race["id"]]["day"] += sum(choice["votes"]) - sum(choice["abs"])
            voting_data[race["id"]]["prov"] += sum(choice["prov"])

            for i, area in enumerate(race["areas"]):
                if ENABLE_AREA_DATA:
                    choice_data["areas"][area] = {
                        "votes": choice["votes"][i],
                        "abs": choice["abs"][i],
                        "day": choice["votes"][i] - choice["abs"][i],
                        "prov": choice["prov"][i],
                    }

                voting_data[race["id"]]["reg"] += turnout_data[area]["reg"]

            voting_data[race["id"]]["choices"].append(choice_data)

        for choice in voting_data[race["id"]]["choices"]:
            choice["percent"] = div_ignore_0(choice["votes"], voting_data[race["id"]]["votes"])

        voting_data[race["id"]]["choices"] = sorted(voting_data[race["id"]]["choices"], key=lambda x: int(x["votes"]), reverse=True)

    send_data_multiple(flatten(voting_data, parent_key="data/gov/frankincounty/vote/current_election_data"))
    send_data_multiple(
        flatten(
            total_turnout,
            parent_key="data/gov/frankincounty/vote/current_election_turnout",
        )
    )

    send_data(data=vote_name, path="data/gov/frankincounty/vote/current_election_name")

    now = datetime.datetime.now(pytz.timezone("US/Eastern"))
    send_data(data=now.strftime("%I:%M:%S %p").lstrip("0"), path="data/gov/frankincounty/vote/last_update_time")

    logger.info("Data refreshed.")

    if scheduler:
        scheduler.enter(10, 1, update_franklincounty_votes, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting gov/franklincounty/vote data_source")

    # delete_keys(search="data/gov/frankincounty/vote/*")

    scheduler = sched.scheduler(time.time, time.sleep)

    scheduler.enter(0, 1, update_franklincounty_votes, argument=(scheduler,))
    scheduler.run()
