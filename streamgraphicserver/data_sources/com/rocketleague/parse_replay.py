# from gevent import monkey; monkey.patch_all()

import glob
import logging
import multiprocessing
import os
import sched
import time
import json

import gevent

import carball
from streamgraphicserver.util.remote_data import send_data, send_data_multiple, delete_keys
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten


def update_data(scheduler=None):
    logger = logging.getLogger(__name__)

    send_data("data/com/rocketleague/replay/status", "Waiting")

    list_of_files = glob.iglob("/mnt/d/shafe/Documents/My Games/Rocket League/TAGame/Demos/*.replay")

    latest_file = max(list_of_files, key=os.path.getctime)
    logger.info("Found: " + latest_file)

    latest_file_json = os.path.splitext(latest_file)[0] + ".json"

    if os.path.isfile(latest_file_json):
        logger.info("Skipping replay as it already has been parsed.")
    else:
        send_data("data/com/rocketleague/replay/status", "Found")
        logger.info("Parsing replay.")
        analysis_manager = carball.analyze_replay_file(latest_file)
        with open(latest_file_json, "w") as json_file:
            proto_game = analysis_manager.write_json_out_to_file(json_file)
        send_data("data/com/rocketleague/replay/status", "Parsed")
        logger.info("Done parsing replay.")

        with open(latest_file_json) as json_file:
            logger.info("Cleaning replay.")
            data = json.load(json_file)

            data["teamsParsed"] = {"orange": {}, "blue": {}}

            for team in data["teams"]:
                del [team["playerIds"]]

                if team["isOrange"]:
                    team_name = "orange"
                else:
                    team_name = "blue"

                # blue=0 orange=1
                data["teamsParsed"][team_name] = team

            data["playersParsed"] = {"orange": [], "blue": []}

            for player in data["players"]:

                if player["isOrange"]:
                    team_name = "orange"
                else:
                    team_name = "blue"

                data["playersParsed"][team_name].append(player)

            del data["teams"]
            del data["players"]
            del data["gameStats"]["hits"]
            del data["gameStats"]["bumps"]
            del data["gameStats"]["kickoffs"]
            del data["gameStats"]["kickoffStats"]
            if "ballCarries" in data["gameStats"]:
                del data["gameStats"]["ballCarries"]
            del data["gameMetadata"]["goals"]
            del data["gameMetadata"]["demos"]

            logger.info("Done cleaning replay.")

            logger.info("Deleting old data.")
            delete_keys(search="data/com/rocketleague/replay/latest/*")

            send_data_multiple(flatten(data, parent_key="data/com/rocketleague/replay/latest"))
            send_data("data/com/rocketleague/replay/status", "Complete")
            logger.info("Done uploading replay.")

    scheduler.enter(15, 1, update_data, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting com/rocketleague/parse_replay data_source")
    scheduler = sched.scheduler(time.time, gevent.sleep)

    scheduler.enter(0, 1, update_data, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    run()
