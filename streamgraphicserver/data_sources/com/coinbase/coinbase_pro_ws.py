import json
import logging
import multiprocessing

from apscheduler.schedulers.background import BackgroundScheduler

from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.other import flatten

logger = logging.getLogger(__name__)

SCHEDULER = BackgroundScheduler()
SCHEDULER.start()

COINBASE_WS_DATA = {}


def batched_update():
    global COINBASE_WS_DATA

    send_data_multiple(flatten(COINBASE_WS_DATA, parent_key="data/com/coinbase/pro/real_time"))


def parse_message(message):
    global COINBASE_WS_DATA

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)
    logger.info(message)

    if message["type"] == "ticker":
        # {
        #     "best_ask": "227.38",
        #     "best_bid": "227.30",
        #     "high_24h": "241.79",
        #     "last_size": "3.79956767",
        #     "low_24h": "220.84",
        #     "open_24h": "241.74",
        #     "price": "227.38",
        #     "product_id": "LTC-USD",
        #     "sequence": 8142515812,
        #     "side": "buy",
        #     "time": "2021-04-24T23:04:04.809817Z",
        #     "trade_id": 62258808,
        #     "type": "ticker",
        #     "volume_24h": "368887.31144232",
        #     "volume_30d": "16437859.02330164",
        # }

        percent_change = (float(message["price"]) - float(message["open_24h"])) / float(message["open_24h"])
        message["percent_change"] = percent_change
        message["percent_change_100"] = percent_change * 100

        message["price_formatted"] = "{:0.2f}".format(float(message["price"]))

        COINBASE_WS_DATA[message["product_id"]] = message


def run():
    from lomond import WebSocket, persist, events

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    SCHEDULER.add_job(batched_update, "interval", seconds=1)

    websocket = WebSocket("wss://ws-feed.pro.coinbase.com")

    logger.info("Connected to Coinbase Pro websocket")

    for event in persist.persist(websocket):
        if isinstance(event, events.Ready):
            websocket.send_text(
                json.dumps(
                    {
                        "type": "subscribe",
                        "product_ids": ["BTC-USD", "ETH-USD", "LTC-USD"],
                        "channels": ["ticker"],
                    }
                )
            )
        if isinstance(event, events.Text):
            parse_message(event.json)

            logger.info("Parsed Coinbase Pro message.")


if __name__ == "__main__":
    logger.error("This should not be run independently.")
    # run()
