import copy
import logging
import multiprocessing
from pprint import pprint

import sched
import time
import gevent

import requests

from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.logging import setup_loggers

logger = logging.getLogger(__name__)

requests_session = requests.Session()


def update_data(scheduler=None, curr_game=None):
    try:
        data = requests.get("https://statsapi.mlb.com/api/v1.1/game/" + str(curr_game) + "/feed/live").json()
        cleaned_data = copy.deepcopy(data)

        del cleaned_data["metaData"]

        del cleaned_data["gameData"]["players"]

        del cleaned_data["liveData"]["plays"]
        del cleaned_data["liveData"]["leaders"]
        del cleaned_data["liveData"]["boxscore"]["officials"]
        del cleaned_data["liveData"]["boxscore"]["info"]
        del cleaned_data["liveData"]["boxscore"]["pitchingNotes"]

        for team_type in ["home", "away"]:
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["players"]
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["batters"]
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["pitchers"]
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["bench"]
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["bullpen"]
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["battingOrder"]
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["info"]
            del cleaned_data["liveData"]["boxscore"]["teams"][team_type]["note"]

        on_base = {"first": False, "second": False, "third": False}

        for pos in ["first", "second", "third"]:
            if pos in cleaned_data["liveData"]["linescore"]["offense"].keys():
                on_base[pos] = True
            else:
                cleaned_data["liveData"]["linescore"]["offense"][pos] = {
                    "fullName": "",
                    "id": None,
                    "link": "",
                }

        cleaned_data["liveData"]["linescore"]["offense"]["on_base_lights"] = on_base

        send_data_multiple(flatten(cleaned_data, parent_key="data/com/mlb/statsapi/viewedgame"))
    except requests.exceptions.ConnectionError:
        pass

    scheduler.enter(10, 1, update_data, argument=(scheduler, curr_game))


def run(game=None):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Running com/mlb/live_game data_source: game=%s", game)

    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s, game))
    s.run()


if __name__ == "__main__":
    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()
