import logging
import multiprocessing
import sched
import time
import gevent

import requests
from streamgraphicserver.util.remote_data import send_data, send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten


def update_data(scheduler, api_key, url, team_id):
    logger = logging.getLogger(__name__)

    session = requests.Session()
    session.trust_env = False

    logger.info("Requesting Buckeyethon Teamraiser update.")
    r = session.get(
        url + "/CRTeamraiserAPI?method=getTeamsByInfo&api_key=" + api_key + "&v=1.0&response_format=json&fr_id=1290&team_id=" + str(team_id),
        timeout=(3.05, 27),
    ).json()

    goal = int(r["getTeamSearchByInfoResponse"]["team"]["goal"]) / 100
    raised = int(r["getTeamSearchByInfoResponse"]["team"]["amountRaised"]) / 100
    progress_val = (raised / goal) * 100
    progress = "{:.2%}".format(raised / goal)

    logger.info("Buckeyethon Teamraiser update: %d/%d (%s)", raised, goal, progress)

    send_data_multiple(
        flatten(
            {
                "goal": goal,
                "raised": raised,
                "progress_val": progress_val,
                "progress": progress,
            },
            parent_key="data/edu/osu/buckeyethon/donation_tracker/team/" + str(team_id),
        )
    )

    scheduler.enter(15, 1, update_data, argument=(scheduler, api_key, url, team_id))


def run(api_key, url, team_id):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting edu/osu/buckeyethon data_source: team_id=%s", team_id)
    scheduler = sched.scheduler(time.time, gevent.sleep)

    scheduler.enter(0, 1, update_data, argument=(scheduler, api_key, url, team_id))
    scheduler.run()


if __name__ == "__main__":
    run(True)
